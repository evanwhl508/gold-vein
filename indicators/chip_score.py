import datetime
import os
import time
import numpy
import matplotlib.pyplot as plt
from decimal import Decimal
from scipy.stats import norm

from config import BASE_DIR
from providers import binance, yahoo
from providers.binance import S_PATH


def price_dist(close, high, low) -> Decimal:
    return (close - (high + low) / 2) ** 2


def sakata_price_dist(open, close, high, low) -> Decimal:
    return (abs(close - open) - (high - low)) ** 2


def adjust_volume(volume, price_dist):
    return volume * price_dist


def weight(total_adjusted_volume, adjusted_volume):
    if total_adjusted_volume == 0 or adjusted_volume == 0:
        return 0
    return adjusted_volume / total_adjusted_volume


"""Volume Weighted Average Price (VWAP): excel King version"""
def vwap(data):
    adjusted_volume = []
    for d in data:
        h = d.max_price
        l = d.min_price
        c = d.close_price
        v = d.trade_volume
        p_d = price_dist(c, h, l)
        a_v = adjust_volume(v, p_d)
        adjusted_volume.append(a_v)
    total_adjusted_volume = sum(adjusted_volume)
    weight_list = [weight(total_adjusted_volume, a_v) for a_v in adjusted_volume]
    _vwap = sum([w * price for w, price in zip(weight_list, [d.close_price for d in data])])
    return _vwap


def sakata_vwap(data):
    adjusted_volume = []
    for d in data:
        h = d.max_price
        l = d.min_price
        o = d.open_price
        c = d.close_price
        v = d.trade_volume
        p_d = sakata_price_dist(o, c, h, l)
        a_v = adjust_volume(v, p_d)
        adjusted_volume.append(a_v)
    total_adjusted_volume = sum(adjusted_volume)
    weight_list = [weight(total_adjusted_volume, a_v) for a_v in adjusted_volume]
    _vwap = sum([w * price for w, price in zip(weight_list, [d.close_price for d in data])])
    return _vwap


def get_stdev(price: list):
    return numpy.std(price, ddof=1)


# strategy 用呢個function
def get_chip_score(data: list, lastest_price, strategy_type):
    """
    from excel王
    excel formula：NORMDIST(x,mean,standard_dev,cumulative)
    x: lastest price
    mean(loc): vwap
    standard_dev (scale): stdev
    cumulative: 如果 cumulative 為 TRUE，則 NORMDIST 會傳回累加分配函數；如果為 FALSE，則會傳回機率質量函數。
    """
    # lastest_price = data[0].close_price
    if strategy_type == "sakata":
        _vwap = sakata_vwap(data)
    else:
        _vwap = vwap(data)
    price_stdev = get_stdev([d.close_price for d in data])  # 計算每組data的價格標準差

    # print(lastest_price, float(lastest_price))
    # print(_vwap, float(_vwap))
    # print(price_stdev,float(price_stdev))
    score = norm.cdf(x=float(lastest_price), loc=float(_vwap),
                     scale=float(price_stdev))  # 累積分布函數 Cumulative Distribution Function
    return score, _vwap


def split(data, days):
    """
    from:[oldest day,...,latest day]
        [1,2,3,4,5,6,7,8,9,10]
    to: [[3, 2, 1], [4, 3, 2], [5, 4, 3], [6, 5, 4], [7, 6, 5], [8, 7, 6], [9, 8, 7], [10, 9, 8]]
        oldest day data group                -->                     latest day data group
    """
    data.reverse()
    split_data = []
    for idx, val in enumerate(data):
        if idx <= len(data) - days:
            buffer = data[idx:idx + days]
            split_data.append(buffer)
    split_data.reverse()
    len_of_remainder_data = len(data) - len(split_data)
    return split_data, len_of_remainder_data


def insert_zero(l: list, count):
    for i in range(count):
        l.insert(0, 0)
    return l


def cal_chips_scores(data, days, strategy_type):
    s_data, num = split(data, days)
    scores = []
    vwaps = []
    for s_d in s_data:
        s, v = get_chip_score(s_d, s_d[0].close_price, strategy_type)
        scores.append(s)
        vwaps.append(v)
        pass
    return insert_zero(scores, num), insert_zero(vwaps, num)


def update_score(symbol, strategy_type="normal", data_type="crypto", save_plot=False):
    if data_type == "crypto":
        data = binance.get_all_record(symbol, S_PATH, 1000, interval='1d')
        if data is None:
            print(f"Wrong symbol.")
            return 0, tuple(), tuple(), "", True
    elif data_type == "us-stock":
        data = yahoo.get_all_record(symbol, interval='1d')
        if data is None:
            print(f"Wrong symbol.")
            return 0, tuple(), tuple(), "", True
    else:
        print(f"Error data type")
        return 0, tuple(), tuple(), ""
    current_price = data[-1].close_price
    x_datatime = [datetime.datetime.fromtimestamp(i.close_time / 1000.0) for i in data]
    y_price = [i.close_price for i in data]

    for d in data:
        d.trade_volume = d.trade_volume * d.close_price
    scores, excel_vwap = cal_chips_scores(data, 100, strategy_type)
    last_10_scores = [f'{score:.3f}' for score in scores[-10:]]

    if save_plot:
        plot_path = save_score_plot(symbol, x_datatime, y_price, scores, excel_vwap)
    else:
        plot_path = None

    return current_price, excel_vwap[-1], last_10_scores, plot_path, False


def get_current_score(data, symbol, strategy_type="normal", data_type="crypto", save_plot=False):
    print(f"data id = {id(data)}")
    if data_type == "crypto":
        for d in data:  # binance volume is using the coin as the unit.
            d.trade_volume = d.trade_volume * d.close_price
        current_price = data[-1].close_price
        x_datatime = [datetime.datetime.fromtimestamp(i.close_time / 1000.0) for i in data]
        y_price = [i.close_price for i in data]
    elif data_type == "us-stock":
        current_price = data[-1].close_price
        x_datatime = [datetime.datetime.fromtimestamp(i.close_time) for i in data]
        y_price = [i.close_price for i in data]
    else:
        print(f"Error data type")
        return dict(), True

    scores, excel_vwap = cal_chips_scores(data, 100, strategy_type)
    last_10_scores = [f'{score:.3f}' for score in scores[-10:]]

    if save_plot:
        plot_path = save_score_plot(symbol, x_datatime, y_price, scores, excel_vwap)
    else:
        plot_path = None

    res = dict(
        current_price=current_price,
        current_vwap=excel_vwap[-1],
        scores_list=last_10_scores,
        plot_path=plot_path,
               )
    return res, False


def save_score_plot(symbol, x_datatime, y_price, y_score, y_excel_king_vwap):
    x_datatime = x_datatime[-100:]
    y_price = y_price[-100:]
    y_excel_king_vwap = y_excel_king_vwap[-100:]
    y_score = y_score[-100:]

    fig, ax1 = plt.subplots()
    fig.set_size_inches(12.66, 5.85)

    ax1.set_xlabel('time (s)')
    ax1.set_ylabel('Price', color='tab:red')
    ax1.plot(x_datatime, y_price, color='tab:red', label=f'{symbol}-price')
    ax1.plot(x_datatime, y_excel_king_vwap, color='green', label='excel_king_vwap')
    ax1.tick_params(axis='y', labelcolor='tab:red')

    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    ax2.set_ylabel('chip score', color='tab:blue')  # we already handled the x-label with ax1
    ax2.plot(x_datatime, y_score, color='tab:blue', label='chip score', linestyle="--")
    ax2.tick_params(axis='y', labelcolor='tab:blue')

    ax2.plot(x_datatime, [0.5 for i in range(len(x_datatime))], color='black', linestyle="--")
    ax2.plot(x_datatime, [0.2 for i in range(len(x_datatime))], color='black', linestyle="--")
    ax2.plot(x_datatime, [0.8 for i in range(len(x_datatime))], color='black', linestyle="--")
    fig.legend(loc='upper left')
    fig.tight_layout()
    path = os.path.join(BASE_DIR,'binance_spot_datas','plot',f"{symbol}_{str(datetime.datetime.now()).replace(':','-')}.png")
    plt.savefig(path, bbox_inches='tight')
    return path

if __name__ == '__main__':
    time_start = time.time()

    #  data = binance.get_all_record('BTCUSDT', S_PATH, 1000, interval='1d')
    #  print('date_close', datetime.datetime.fromtimestamp(data[-2].close_time / 1000.0))
    #  price_stdev = get_stdev([d.close_price for d in data[-101:-2]])
    #  for d in data:
    #      d.trade_volume = d.trade_volume * d.close_price
    #  scores,excel_vwap = cal_chips_scores(data, 100)
    #
    # #datetime.datetime.fromtimestamp(data[-1].close_time/1000.0)
    #  print('score',scores[-10:])
    #  print('excek king vwap',excel_vwap[-2],excel_vwap[-1])
    #  print('sd',price_stdev)
    # current_price, excel_vwap, last_10_scores = update_score('ETH USDT')
    # print(current_price, excel_vwap, last_10_scores)

    update_score('ETHUSDT', True)

    time_end = time.time()
    print('time cost', time_end - time_start, 's')
