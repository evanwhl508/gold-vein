import datetime
import matplotlib.pyplot as plt
import os

from config import BASE_DIR
from providers import binance, yahoo
from providers.binance import S_PATH


def insert_zero(l: list, count):
    for i in range(count):
        l.insert(0, 0)
    return l


def cal_donchian_channel(data, enter_days, exit_days):
    highest = []
    lowest = []
    close_price = []
    # sma = []
    upper_in = []
    upper_out = []
    lower_in = []
    lower_out = []
    len_of_remained_data = 0

    # data.reverse()  # (latest --> oldest) change to (oldest --> latest)
    for d in data:
        h = d.max_price
        l = d.min_price
        c = d.close_price
        highest.append(h)
        lowest.append(l)
        close_price.append(c)
        if len(close_price) <= enter_days:
            len_of_remained_data += 1
            continue
        # today_sma = sum(close_price[-days:]) / days
        # sma.append(today_sma)
        upper_in.append(max(highest[-enter_days:-1]))
        lower_in.append(min(lowest[-enter_days:-1]))
        upper_out.append(min(lowest[-exit_days:-1]))
        lower_out.append(max(highest[-exit_days:-1]))
    upper_in = insert_zero(upper_in, len_of_remained_data)
    lower_in = insert_zero(lower_in, len_of_remained_data)
    upper_out = insert_zero(upper_out, len_of_remained_data)
    lower_out = insert_zero(lower_out, len_of_remained_data)
    # sma = insert_zero(sma, len_of_remained_data)
    return upper_in, upper_out, lower_in, lower_out


def get_donchian(symbol, enter_days, exit_days, data_type="crypto", save_plot=False):
    if data_type == "crypto":
        data = binance.get_all_record(symbol, S_PATH, 1000, interval='1d')
    elif data_type == "us-stock":
        data = yahoo.get_all_record(symbol, 1000, interval='1d')
    else:
        print(f"Error data type")
        return 0, tuple(), tuple(), "", True
    if data is None:
        print(f"Wrong symbol.")
        return 0, tuple(), tuple(), "", True
    current_price = data[-1].close_price
    x_datatime = [datetime.datetime.fromtimestamp(i.close_time / 1000.0) for i in data]
    y_price = [i.close_price for i in data]

    for d in data:
        d.trade_volume = d.trade_volume * d.close_price  # volume is in terms of the coin
    upper_in, upper_out, lower_in, lower_out = cal_donchian_channel(data, enter_days, exit_days)
    # last_10_scores = [f'{score:.3f}' for score in scores[-10:]]

    plot_path = None
    if save_plot:
        plot_path = save_score_plot(symbol, x_datatime, y_price, upper_in, upper_out, lower_in, lower_out)
    else:
        plot_path = None

    return current_price, (upper_in[-1], upper_out[-1]), (lower_in[-1], lower_out[-1]), plot_path, False


def save_score_plot(symbol, x_datatime, y_price, y_upper_in, y_upper_out, y_lower_in, y_lower_out):
    x_datatime = x_datatime[-100:]
    y_price = y_price[-100:]
    y_upper_in = y_upper_in[-100:]
    y_upper_out = y_upper_out[-100:]
    y_lower_in = y_lower_in[-100:]
    y_lower_out = y_lower_out[-100:]

    fig, ax1 = plt.subplots()
    fig.set_size_inches(12.66, 5.85)
    fig.suptitle(f'{symbol} - Donchian Channel', fontsize=6)

    ax1.set_xlabel('time (s)')
    ax1.set_ylabel('Price', color='tab:red')
    ax1.plot(x_datatime, y_price, color='tab:red', label=f'price')
    ax1.plot(x_datatime, y_upper_in, color='green', label='upper-in')
    ax1.plot(x_datatime, y_upper_out, color='green', label='upper-out')
    ax1.tick_params(axis='y', labelcolor='tab:red')
    ax1.plot(x_datatime, y_lower_in, color='tab:blue', label='lower-in', linestyle="--")
    ax1.plot(x_datatime, y_lower_out, color='tab:blue', label='lower-out', linestyle="--")
    ax1.tick_params(axis='y', labelcolor='tab:blue')

    # ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    # ax2.set_ylabel('chip score', color='tab:blue')  # we already handled the x-label with ax1
    # ax2.plot(x_datatime, y_lower, color='tab:blue', label='lower', linestyle="--")
    # ax2.tick_params(axis='y', labelcolor='tab:blue')

    # ax2.plot(x_datatime, [0.5 for i in range(len(x_datatime))], color='black', linestyle="--")
    # ax2.plot(x_datatime, [0.2 for i in range(len(x_datatime))], color='black', linestyle="--")
    # ax2.plot(x_datatime, [0.8 for i in range(len(x_datatime))], color='black', linestyle="--")
    fig.legend(loc='upper left')
    fig.tight_layout()
    path = os.path.join(BASE_DIR,'binance_spot_datas','plot',f"{symbol}_{str(datetime.datetime.now()).replace(':','-')}.png")
    plt.savefig(path, bbox_inches='tight')
    return path


if __name__ == '__main__':
    get_donchian("BTCUSDT")
