import os
from csv import DictReader, DictWriter
from dataclasses import dataclass, field
from datetime import datetime
from decimal import Decimal
from typing import List

from models import LimitOrder, Direction, DataStream, TradeLog, Data
from providers import alpha_vantage, binance


@dataclass
class Account:
    initial_cash_amount: Decimal
    cash_balance: Decimal = 0
    asset_balance: Decimal = 0
    orders: List[LimitOrder] = field(default_factory=list)
    trade_logs: List[TradeLog] = field(default_factory=list)

    def __post_init__(self):
        self.cash_balance = self.initial_cash_amount

    @property
    def open_orders(self):
        return [x for x in self.orders if x.is_open]

    @property
    def filled_orders(self):
        return sorted([x for x in self.orders if not x.is_open], key=lambda x: x.filled_dt)

    @property
    def locked_cash_amount(self):
        return sum(x.cash_amount for x in self.open_orders if x.direction == Direction.BUY)

    @property
    def idle_cash_amount(self):
        return self.cash_balance - self.locked_cash_amount

    def get_value(self, asset_price):
        return self.cash_balance + self.asset_balance * asset_price

    def validate(self):
        assert self.idle_cash_amount >= 0

    def create_order(self, symbol, price, direction, order_dt: datetime, amount):
        order = LimitOrder(
            order_dt=order_dt,
            symbol=symbol,
            price=price,
            direction=direction,
            amount=amount,
        )
        self.orders.append(order)
        return order

    def execute_order(self, order: LimitOrder, dt):
        print(f'{dt} {order.direction.name} {order.symbol} @ {order.price} for {order.amount}')
        cash_change = order.amount * order.price
        if order.direction == Direction.BUY:
            assert self.cash_balance >= cash_change, f"Required {cash_change}, got {self.cash_balance}"
            self.cash_balance -= cash_change
            self.asset_balance += order.amount
        elif order.direction == Direction.SELL:
            assert self.asset_balance >= order.amount, f"Required {order.amount}, got {self.asset_balance}"
            self.cash_balance += cash_change
            self.asset_balance -= order.amount
        else:
            raise ValueError()
        order.execute(dt)
        print(self.cash_balance)
        self.trade_logs.append(TradeLog(
            dt=dt,
            order_dt=order.order_dt,
            trade_price=order.price,
            trade_amount=order.amount,
            direction=order.direction,
            cash_balance=self.cash_balance,
            asset_balance=self.asset_balance,
        ))

    def output_csv(self):
        dict_list = [x.as_dict() for x in sorted(self.trade_logs, key=lambda x: x.order_dt)]
        writer = DictWriter(open('out2.csv', 'w', newline=''), fieldnames=dict_list[0].keys())
        writer.writeheader()
        for row in dict_list:
            writer.writerow(row)


@dataclass
class GridStrategy:
    symbol: str
    lower_limit: Decimal
    upper_limit: Decimal
    initial_price: Decimal
    initial_dt: datetime
    grid_count: int
    grid_amount: Decimal
    decimal_place: int
    account: Account = None

    def set_account(self, account):
        self.account = account

    @property
    def total_width(self):
        return self.upper_limit - self.lower_limit

    @property
    def grid_width(self):
        return self.total_width / (self.grid_count - 1)

    @property
    def grids(self):
        raw = [self.lower_limit + x * self.grid_width for x in range(self.grid_count)]
        base = 10 ** self.decimal_place
        return [Decimal(str(int(x * base) / base)) for x in raw]

    @property
    def initial_grid(self):
        min_diff = min(abs(x-self.initial_price) for x in self.grids)
        return next(x for x in self.grids if abs(x-self.initial_price) == min_diff)

    def output_report(self, last_data: Data):
        final_value = self.account.get_value(last_data.close)
        initial_value = self.account.initial_cash_amount
        total_profit = final_value - initial_value
        grid_profit = self.account.idle_cash_amount
        period = (last_data.dt - self.initial_dt).days
        tran_count = len(self.account.filled_orders)
        output = [
            'Parameters : '
            f'symbol = {self.symbol}',
            f'lower limit = {self.lower_limit}',
            f'upper limit = {self.upper_limit}',
            f'grid count = {self.grid_count}',
            f'grid width = {self.grid_width:.2f}',
            '',
            'Result : ',
            f'initial value = {initial_value:.2f}',
            f'final value = {final_value:.2f}',
            f'transaction = {tran_count}',
            f'initial price = {self.initial_price:.2f}',
            f'final price = {last_data.close:.2f}',
            f'grid profit = {grid_profit:.2f}',
            f'total profit = {total_profit:.2f}',
            f'grid profit rate = {grid_profit/initial_value:.2%}',
            f'total profit rate = {total_profit/initial_value:.2%}',
            f'start dt = {self.initial_dt}',
            f'end dt = {last_data.dt}',
            f'period = {period} days',
        ]
        for row in output:
            print(row)

    def output_orders(self):
        for row in self.account.filled_orders:
            print(row.order_dt, row.filled_dt, row.amount, row.price, row.direction.value, sep='\t')

    def validate(self):
        open_orders = self.get_open_orders()
        assert len(open_orders) == self.grid_count - 1

    def create_order(self, price, direction, order_dt: datetime, amount):
        return self.account.create_order(self.symbol, price, direction, order_dt, amount)

    def create_grid_order(self, price, direction, order_dt: datetime):
        return self.create_order(price, direction, order_dt, amount=self.grid_amount)

    def execute_order(self, order: LimitOrder, dt):
        self.account.execute_order(order, dt)

    def spawn_order(self, from_order: LimitOrder):
        assert not from_order.is_open
        index = self.grids.index(from_order.price)
        assert index >= 0
        if from_order.direction == Direction.BUY:
            grid = self.grids[index+1]
            direction = Direction.SELL
        elif from_order.direction == Direction.SELL:
            grid = self.grids[index-1]
            direction = Direction.BUY
        else:
            raise ValueError()
        self.create_grid_order(grid, direction=direction, order_dt=from_order.filled_dt)

    def get_orders(self):
        return self.account.orders

    def get_open_orders(self):
        return [x for x in self.get_orders() if x.is_open]

    def compute_grid_profit(self):
        grids = self.grids
        highest = self.grid_width / grids[0]
        lowest = self.grid_width / grids[-1]
        return f'{lowest:.3%}-{highest:.3%}'

    def compute_required(self):
        grids = self.grids
        grids.remove(self.initial_grid)
        grids_lower = [x for x in grids if x < self.initial_price]
        grids_upper = [x for x in grids if x > self.initial_price]
        return grids_upper, grids_lower

    def compute_required_cash(self):
        grids_upper, grids_lower = self.compute_required()
        lower_cash = sum(x * self.grid_amount for x in grids_lower)
        upper_cash = len(grids_upper) * self.grid_amount * self.initial_price
        return lower_cash + upper_cash

    def buy_initial_assets(self, initial_amount):
        initial_order = self.create_order(
            self.initial_price,
            Direction.BUY,
            self.initial_dt,
            amount=initial_amount,
        )
        self.execute_order(initial_order, self.initial_dt)

    def initiate(self):
        grids_upper, grids_lower = self.compute_required()
        initial_amount = len(grids_upper) * self.grid_amount
        self.buy_initial_assets(initial_amount=initial_amount)
        upper_orders = [self.create_grid_order(x, Direction.SELL, self.initial_dt) for x in grids_upper]
        lower_orders = [self.create_grid_order(x, Direction.BUY, self.initial_dt) for x in grids_lower]
        # self.orders.extend(upper_orders)
        # self.orders.extend(lower_orders)


def get_stream(symbol):
    path = os.path.join('data', f'alpha_vantage_{symbol.lower()}.csv')
    reader = DictReader(open(path))
    data_stream = DataStream([alpha_vantage.parse(x, symbol) for x in reader])
    return data_stream


def get_stream_binance(symbol):
    raw_list = binance.load_spot_data(symbol)
    data_stream = DataStream([binance.parse(x) for x in raw_list])
    return data_stream


def compare():
    st = GridStrategy(
        lower_limit=Decimal('476.88'),
        upper_limit=Decimal('650.37'),
        initial_price=Decimal('584.71'),
        grid_count=54,
        grid_amount=Decimal('1'),
        initial_dt=datetime.now(),
        symbol='ETH',
        decimal_place=2,
    )
    print(st.compute_grid_profit())


def run():
    # symbol = 'AAPL'
    # stream = get_stream(symbol)
    symbol = 'ETHBUSD'
    stream = get_stream_binance(symbol)
    stream.set_range(
        from_dt=datetime(2020, 12, 13, 23, 32, 11),
        to_dt=datetime(2020, 12, 16, 22, 00, 32)
    )
    for row in stream.data_list:
        print(row)
    print(stream.open, stream.close, stream.highest, stream.lowest)
    # input('hi')
    # st = GridStrategy(
    #     lower_limit=Decimal('113'),
    #     upper_limit=Decimal('125'),
    #     initial_price=stream.open,
    #     grid_count=15,
    #     grid_amount=Decimal('3'),
    #     initial_dt=stream.open_dt,
    #     symbol=symbol,
    # )
    st = GridStrategy(
        lower_limit=Decimal('581.09'),
        upper_limit=Decimal('609.78'),
        initial_price=stream.open,
        grid_count=11,
        grid_amount=Decimal('7'),
        initial_dt=stream.open_dt,
        symbol=symbol,
        decimal_place=2,
    )
    required_cash = st.compute_required_cash()
    account = Account(initial_cash_amount=required_cash)
    st.set_account(account)
    print(st.grids)
    # input('hhh')
    st.initiate()
    print(st)
    for row in st.get_orders():
        print(row)
    print(st.compute_grid_profit())
    for data in stream.data_list:
        st.validate()
        account.validate()
        open_orders = st.get_open_orders()
        buy_orders = [x for x in open_orders if x.direction == Direction.BUY]
        sell_orders = [x for x in open_orders if x.direction == Direction.SELL]
        if data.high > data.open:
            for order in sell_orders:
                if data.high > order.price:
                    st.execute_order(order, dt=data.dt)
                    st.spawn_order(from_order=order)
        if data.low < data.open:
            for order in buy_orders:
                if data.low < order.price:
                    st.execute_order(order, dt=data.dt)
                    st.spawn_order(from_order=order)
    st.output_report(stream.close_tick)
    account.output_csv()


if __name__ == '__main__':
    run()
    # compare()
