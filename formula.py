def compute_share_num(total_equity, risk_percent, atr):
    # 目的: 當價位向一個ATR反方向走, 損失是risk_percent
    return (total_equity * risk_percent) / atr

def compute_amount(total_equity, price):
    print(total_equity / price)
    return total_equity / price


def compute_add_unit_price(base_price, atr, is_long):
    mul = 1 if is_long else -1
    return base_price + mul * atr / 2
