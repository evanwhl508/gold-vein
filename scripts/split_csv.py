import json
import logging
import os
import pathlib
import time

import pandas as pd
import zipfile


logger = logging.getLogger(__name__)
input_folder = 'D:/stockData/'
output_folder = 'D:/stockData/output/'
checklist_path = 'checklist.json'


def load_data_path(file_dir):
    file_list = []
    for root, _, file_names in os.walk(file_dir):
        for file_name in file_names:
            ext = pathlib.Path(file_name).suffix
            if ext == '.zip':
                path = os.path.join(root, file_name)
                file_list.append(path)
    return file_list


def load_checklist(path=checklist_path):
    if os.path.exists(path):
        try:
            with open(path, 'r')as f:
                check_list = json.load(f)
                return check_list
        except Exception as e:
            return {}
    return {}


def load_from_zip(input_path, file_name, ext='csv'):
    zf = zipfile.ZipFile(input_path)
    """symbol maybe not in path"""
    df = pd.read_csv(zf.open(f'{file_name}.{ext}'), dtype={'Time': str})
    return df


def split_csv_to_single_year(input_path, output_path, check_list=None, specific_year=None):
    skip_years = []
    if not check_list:
        check_list = {}
    symbol = pathlib.Path(input_path).stem  # get file name without extension
    category = os.path.basename(os.path.dirname(input_path))  # get data category from it's folder name
    if category == 'Dow Jones Industrial Components':
        category = 'Stocks'

    df = load_from_zip(input_path, symbol, 'txt')
    start_year = int(df.head(1)['Date'].values[0][-4:])
    end_year = int(df.tail(1)['Date'].values[0][-4:])
    # print(df)
    # print(start_year)
    # print(end_year)

    """set index with Date for split csv with years"""
    df['index'] = pd.to_datetime(df['Date'])  # Create new column
    df = df.set_index(df['index'])  # set index
    del df['index']  # del column

    """remark for record, checklist consist of remarks"""
    remark = {symbol: {"from": start_year,
                       "to": end_year,
                       "category":category,
                       "split_file": {},
                       }}
    """check split_file for skip"""
    if symbol in check_list and specific_year is None:
        remark[symbol] = check_list[symbol]  # get old record
        finished_years = [int(i) for i in check_list[symbol]['split_file']]
        finished_years.sort()
        if finished_years:  # split_file not empty
            if finished_years == [i for i in range(start_year, end_year + 1)]:
                print("="*10,"skip", symbol, "="*10)
                """file has been split,not thing change, skip"""
                return check_list
            else:
                skip_years = finished_years

    if specific_year and start_year <= specific_year <= end_year:
        remark = check_list[symbol]
        start_year = specific_year
        end_year = specific_year

    """loop years to split the file"""
    try:
        for year in range(start_year, end_year + 1):
            if year in skip_years:
                print("="*10,'skip', symbol,' ', year, "="*10)
                continue
            print("#"*10,f"start split {symbol} year", year, "#"*10)
            buffer_df = df[f"{year}"]
            row = len(buffer_df)  # not include title row

            """save df to csv"""
            split_file_path = os.path.join(output_path, f'{symbol}_{year}.csv')
            buffer_df.to_csv(split_file_path, index=False)  # save
            """write checklist"""
            remark[symbol]['split_file'][year] = row
            check_list[symbol] = remark[symbol]
            with open(checklist_path, 'w') as f:
                json.dump(check_list, f, indent=4)
            """print completed process"""
            print(f"Save: {split_file_path}")
    except Exception as e:
        logger.error(f'on split {symbol}: {e},and skip {symbol}')
    return check_list


def split_all():
    files = load_data_path(input_folder)
    if not files:
        logger.warning(f"There is no file in {input_folder}")
        return
    check_list = load_checklist()

    for file in files:
        check_list = split_csv_to_single_year(file, output_folder, check_list)

    return check_list


def check_all(check_list):
    files = load_data_path(input_folder)  # get all symbol names
    unfinished_stock = []

    for file in files:
        symbol = pathlib.Path(file).stem
        if symbol in check_list:
            years = [int(i) for i in check_list[symbol]['split_file']]
            years.sort()
            if years:  # split_file not empty
                if not years == [i for i in range(check_list[symbol]['from'], check_list[symbol]['to'] + 1)]:  # check last split_file = to year
                    unfinished_stock.append(symbol)
                    continue
            for year, row in check_list[symbol]['split_file'].items():  # check is there any missing data
                path = os.path.join(output_folder, f"{symbol}_{year}.csv")
                df = pd.read_csv(path)
                if not row == len(df):
                    logger.error(f"{symbol}_{year}: origin file rows {row}, rows of split files {len(df)}")
                    if symbol not in unfinished_stock:
                        unfinished_stock.append({f"{symbol}_{year}"})
        else:
            unfinished_stock.append(symbol)
    return unfinished_stock


if __name__ == '__main__':
    time_start = time.time()

    result = split_all()
    print(f"unfinished: {check_all(load_checklist())}")

    time_end = time.time()
    print('time cost', time_end - time_start, 's')
