import pathlib
import zipfile

from pyalgotrade.tools import resample

from backtest.pyalgotrade.meta import Frequency
from pi import PiTradingFeed

ROOT_DIR = pathlib.Path(__file__).parent.parent
ZIP_FILE_PATH = pathlib.Path(ROOT_DIR, 'output.zip')
TARGET_PATH = pathlib.Path(ROOT_DIR, 'resample_daily')


def resample_csv(instrument, year, target_path, frequency=Frequency.DAY):
    feed = PiTradingFeed(frequency)
    feed.addBarsAuto(instrument, from_year=year)
    resample.resample_to_csv(feed, frequency, target_path)


if __name__ == '__main__':
    with zipfile.ZipFile(ZIP_FILE_PATH, 'r') as zf:
        filename_list = zf.namelist()
        for filename in filename_list:
            row = filename.replace('.', '_').split("_")
            symbol = row[0]
            year = int(row[1])
            path = pathlib.Path(TARGET_PATH, f'{symbol}_daily_{year}.csv')
            resample_csv(symbol, year, target_path=path)
