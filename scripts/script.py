import itertools

from backtest.chip_score.chip_score_strategy import ChipScoreStrategy
from backtest.pyalgotrade import service
from backtest.pyalgotrade.atr_channel import ATRChannelStrategy
from backtest.pyalgotrade.bollinger_breakout import BollingerBreakout
from backtest.pyalgotrade.donchian_channel import DonchianChannelStrategy
from backtest.pyalgotrade.donchian_channel_with_time_exit import DonchianChannelWithTimeExit
from backtest.pyalgotrade.dual_moving_average import DualMovingAverage
from backtest.pyalgotrade.meta import Frequency
from backtest.pyalgotrade.triple_moving_average import TripleMovingAverage
from pi import PiTradingFeed

STRATEGIES = [
    "ATRChannelStrategy",
    "BollingerBreakout",
    "DonchianChannelStrategy",
    "DonchianChannelWithTimeExit",
    "DualMovingAverage",
    "TripleMovingAverage",
    "ChipScoreStrategy",
]

MAPPING_FIELDS = {
    "ATRChannelStrategy": ["invest_percentage_list", "stop_loss_atr_list", "day_list"],
    "BollingerBreakout": ["invest_percentage_list", "stop_loss_atr_list", "day_list"],
    "DonchianChannelStrategy": ["invest_percentage_list", "stop_loss_atr_list", "high_low_period"],
    "DonchianChannelWithTimeExit": ["invest_percentage_list", "stop_loss_atr_list", "high_low_period"],
    "DualMovingAverage": ["invest_percentage_list", "dma"],
    "TripleMovingAverage": ["invest_percentage_list", "tma"],
    "ChipScoreStrategy": ["invest_percentage_list", "stop_loss_atr_list", "day_list", "score_setting", "position_side"],
    # "TurtleSystem1Strategy": ["invest_percentage_list", "stop_loss_atr_list"],
    # "TurtleSystem2Strategy": ["invest_percentage_list", "stop_loss_atr_list"],
}

VARIABLES = {
    "invest_percentage_list": [0.005, 0.01, ],
    "stop_loss_atr_list": [0, 1, 2, ],
    "day_list": [20, 100, 350],
    "high_low_period": [20, 100, 200],
    "dma": [(5, 10), (20, 50), (50, 100), (120, 240), (100, 250)],
    "tma": [(5, 20, 50), (20, 50, 100), (60, 120, 240), (100, 250, 350), ],
    "score_setting": [(0.8, 0.5, 0.2), (0.7, 0.5, 0.3), (0.6, 0.5, 0.4), (0.5, 0.5, 0.5)],
    "position_side": [(True, True), (True, False), (False, True)],  # Long Short
}

TO_YEAR = 2020


def call_strategy(strat_name, feed, instrument, parameter):
    if strat_name == "ATRChannelStrategy":
        invest_percentage = parameter[0]
        stop_loss = parameter[1]
        day = parameter[2]
        strat, analy = ATRChannelStrategy.run_strategy(
            feed=feed,
            config=dict(
                instrument=instrument,
                cash_or_brk=1000000,
                allow_short=True,
                allow_long=True,
                atr_period=20,
                # variables
                stop_loss_atr=stop_loss,
                invest_percentage=invest_percentage,
                sma_day=day,
            ),
            plot_graph=False,
        )
    elif strat_name == "BollingerBreakout":
        invest_percentage = parameter[0]
        stop_loss = parameter[1]
        day = parameter[2]
        strat, analy = BollingerBreakout.run_strategy(
            feed=feed,
            config=dict(
                instrument=instrument,
                cash_or_brk=1000000,
                allow_short=True,
                allow_long=True,
                # variables
                stop_loss_atr=stop_loss,
                invest_percentage=invest_percentage,
                sma_day=day,
            ),
            plot_graph=False,
        )
    elif strat_name == "DonchianChannelStrategy":
        invest_percentage = parameter[0]
        stop_loss = parameter[1]
        high_low_period = parameter[2]
        strat, analy = DonchianChannelStrategy.run_strategy(
            feed=feed,
            config=dict(
                instrument=instrument,
                cash_or_brk=1000000,
                allow_short=True,
                allow_long=True,
                atr_period=20,
                sma1_period=25,
                sma2_period=350,
                # variables
                stop_loss_atr=stop_loss,
                invest_percentage=invest_percentage,
                high_low_period=high_low_period,
            ),
            plot_graph=False,
        )
    elif strat_name == "DonchianChannelWithTimeExit":
        invest_percentage = parameter[0]
        stop_loss = parameter[1]
        high_low_period = parameter[2]
        strat, analy = DonchianChannelWithTimeExit.run_strategy(
            feed=feed,
            config=dict(
                instrument=instrument,
                cash_or_brk=1000000,
                allow_short=True,
                allow_long=True,
                exit_period=28800,
                atr_period=20,
                sma1_period=25,
                sma2_period=350,
                # variables
                stop_loss_atr=stop_loss,
                invest_percentage=invest_percentage,
                high_low_period=high_low_period,
            ),
            plot_graph=False,
        )
    elif strat_name == "DualMovingAverage":
        invest_percentage = parameter[0]
        short_sma_period, long_sma_period = parameter[1]
        strat, analy = DualMovingAverage.run_strategy(
            feed=feed,
            config=dict(
                instrument=instrument,
                cash_or_brk=1000000,
                allow_short=True,
                allow_long=True,
                # variables
                invest_percentage=invest_percentage,
                short_sma_period=short_sma_period,
                long_sma_period=long_sma_period,
            ),
            plot_graph=False,
        )
    elif strat_name == "TripleMovingAverage":
        invest_percentage = parameter[0]
        short_sma_period, long_sma_period, trend_sma_period = parameter[1]
        strat, analy = TripleMovingAverage.run_strategy(
            feed=feed,
            config=dict(

                instrument=instrument,
                cash_or_brk=1000000,
                allow_short=True,
                allow_long=True,
                # variables
                invest_percentage=invest_percentage,
                short_sma_period=short_sma_period,
                long_sma_period=long_sma_period,
                trend_sma_period=trend_sma_period,
            ),
            plot_graph=False,
        )
    elif strat_name == "ChipScoreStrategy":
        invest_percentage = parameter[0]
        stop_loss = parameter[1]
        day = parameter[2]
        long_score, middle_score, short_score = parameter[3]
        allow_long, allow_short = parameter[4]
        strat, analy = ChipScoreStrategy.run_strategy(
            feed=feed,
            config=dict(
                instrument=instrument,
                cash_or_brk=1000000,
                atr_period=20,
                stop_loss_atr=stop_loss,
                all_in_spot=False,
                # variables
                allow_long=allow_long,
                allow_short=allow_short,
                score_days=day,
                invest_percentage=invest_percentage,
                long_score=long_score,
                middle_score=middle_score,
                short_score=short_score,
            ),
            plot_graph=False,
        )

    else:
        raise ValueError("No matched strategy.")
    return strat, analy


def get_variables(fields) -> dict:
    res = {}
    for field in fields:
        res[field] = VARIABLES[field]
    return res


if __name__ == '__main__':
    for strategy_name in STRATEGIES:
        data_patterns = list(get_variables(MAPPING_FIELDS[strategy_name]).values())
        data_set = list(itertools.product(*data_patterns))
        for row in service.get_pi_stock_list(TO_YEAR):
            symbol, from_year = row
            if from_year < 2001:
                from_year = 2001
            for data in data_set:
                feed = PiTradingFeed(frequency=Frequency.DAY)
                feed.addBarsAuto(instrument=symbol, from_year=from_year, to_year=TO_YEAR)
                # start looping
                strategy, analyzer = call_strategy(strategy_name, feed, symbol, data)
                del strategy
                del analyzer
