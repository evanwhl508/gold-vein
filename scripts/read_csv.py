import logging
import os
import pathlib
import time
import zipfile
from typing import List

import pandas as pd

from backtest.pyalgotrade.meta import Frequency
from scripts import meta
from scripts.split_csv import load_checklist, load_from_zip

logger = logging.getLogger(__name__)

ROOT_DIR = pathlib.Path(__file__).parent.parent
ZIP_FILE_PATH = pathlib.Path(ROOT_DIR, 'output.zip')
RESAMPLE_ZIP_FILE_PATH = pathlib.Path(ROOT_DIR, 'resample_daily.zip')
FOUR_HOUR_ZIP_FILE_PATH = pathlib.Path(ROOT_DIR, '4hr_resample.zip')
EXTRACT_PATH = pathlib.Path(ROOT_DIR, 'temp')


def get_symbols(categories: List[meta.CategoryType], check_list=load_checklist()):
    symbols = []
    for category in categories:
        symbols.extend([key for key, value in check_list.items() if value['category'].lower() == category])
    return symbols


def get_filename(frequency, symbol, year):
    if frequency == Frequency.DAY:
        filename = f"{symbol.upper()}_daily_{year}.csv"
    elif frequency == Frequency.MINUTE:
        filename = f"{symbol.upper()}_{year}.csv"
    elif frequency == Frequency.FOUR_HOUR:
        filename = f"{symbol.upper()}_4hr_{year}.csv"
    else:
        raise
    return filename


def get_split_csv_path(symbols: List[str], frequency, from_year: int,
                       to_year: int = None, del_old_file=True, extract_folder=EXTRACT_PATH):
    if del_old_file:
        if get_size() > 1000000000:  # >1GB
            del_csv()

    if frequency == Frequency.DAY:
        input_zip_path = RESAMPLE_ZIP_FILE_PATH
    elif frequency == Frequency.MINUTE:
        input_zip_path = ZIP_FILE_PATH
    elif frequency == Frequency.FOUR_HOUR:
        input_zip_path = FOUR_HOUR_ZIP_FILE_PATH
    else:
        raise

    path_list = []
    if not to_year or to_year < from_year:
        to_year = from_year

    with zipfile.ZipFile(input_zip_path, 'r') as zf:
        namelist = zf.namelist()
        for symbol in symbols:
            for year in [_year for _year in range(from_year, to_year + 1)]:
                filename = get_filename(frequency, year=year, symbol=symbol)
                if filename in namelist:  # check the csv inside the zip.
                    if not os.path.exists(os.path.join(extract_folder, filename)):
                        file_path = zf.extract(member=filename, path=extract_folder)  # extract
                        path_list.append(file_path)
                        print("*" * 10, f"Extract {filename} to temp file", "*" * 10)
                    else:  # if the CSV has been extracted, just add path.
                        path_list.append(os.path.join(extract_folder, filename))
                        logger.warning(f"{filename} already exists, skip extract.")
                else:
                    logger.warning(f"{symbol} {year}'s data doesn't exist.")
    return path_list


def read_from_zip(file_name, input_zip_path=ZIP_FILE_PATH):
    with zipfile.ZipFile(input_zip_path, 'r') as zf:
        namelist = zf.namelist()
        if file_name in namelist:
            data = zf.read(file_name).decode('utf8')
            return data  # read directly from zip without extract


def del_csv(path=EXTRACT_PATH):
    for f in os.listdir(path):
        if not f.endswith(".csv"):
            continue
        os.remove(os.path.join(path, f))


def get_size(path=EXTRACT_PATH):
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            if not os.path.islink(fp):
                total_size += os.path.getsize(fp)
    return total_size  # bytes, 1 gigabyte = 1,000,000,000 bytes


def read_split_csv_to_df(symbol, year, input_path=ZIP_FILE_PATH, check_list: dict = None,
                         generate_timestamp=False) -> pd.DataFrame or None:
    symbol = symbol.upper()
    # check before load
    if check_list:
        if symbol not in check_list:
            logger.error(f'There are no {symbol} data')
            return
        elif not check_list[symbol]['from'] <= year <= check_list[symbol]['to']:
            logger.error(
                f"{symbol} data From {check_list[symbol]['from']} To {check_list[symbol]['to']}, {year}'s data doesn't exist.")
            return

    try:
        ext = pathlib.Path(input_path).suffix
        if ext == '.zip':
            df = load_from_zip(input_path, f"{symbol}_{year}")
        else:
            path = os.path.join(input_path, f'{symbol}_{year}.csv')
            df = pd.read_csv(path, dtype={'Time': str})
    except Exception as e:
        logger.error(f"Exception: {e}")
        return

    if generate_timestamp:  # wasting time
        df['Timestamp'] = pd.to_datetime(df['Date'] + ' ' + df['Time'])
        df = df.set_index(df['Timestamp'])
        del df['Timestamp']

    return df


if __name__ == '__main__':
    time_start = time.time()

    # check_list = load_checklist()
    # df = read_split_csv_to_df('AAPL', 1999, check_list)

    all_symbols = [category.value for category in meta.CategoryType]
    symbols_1 = get_symbols([meta.CategoryType.ETF.value])
    symbols_2 = ['aapl', 'GOOGL']
    csv_path = get_split_csv_path(symbols_2, Frequency.FOUR_HOUR, 2012, 2020)
    print(len(csv_path))
    print(csv_path)

    # ====
    time_end = time.time()
    print('time cost', time_end - time_start, 's')
