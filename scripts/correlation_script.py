import os
from collections import namedtuple
from datetime import datetime
from typing import Dict

from openpyxl import load_workbook, Workbook
from pyalgotrade import strategy
from pyalgotrade.bar import Bar, Bars

from backtest.pyalgotrade import service
from backtest.pyalgotrade.meta import Frequency
from config import LOG_DIR
from pi import PiTradingFeed

Price = namedtuple('Price', ['year', 'month', 'price'])


class Correlation(strategy.BacktestingStrategy):

    def __init__(self, feed, cash_or_brk, from_dt, to_dt, symbols):
        super().__init__(feed, cash_or_brk)
        self.from_dt = from_dt
        self.to_dt = to_dt
        self.symbols = symbols
        self.returns = {symbol: [] for symbol in self.symbols}
        self.month_start_prices = None
        self.month_end_prices = None
        self.month_begin_dt = None
        self.month_end_dt = None
        self.is_start = {symbol: True for symbol in self.symbols}
        self.last_bars = dict()  # type: Dict[str, Bar]
        self.current_bar = None  # type: Bar
        self.reset(all=True)

    def reset(self, all=False, symbol=None):
        if all:
            self.month_start_prices = {symbol: [] for symbol in self.symbols}
            self.month_end_prices = {symbol: [] for symbol in self.symbols}
        else:
            if symbol is None:
                raise Exception('Symbol cannot be none')
            self.month_start_prices[symbol] = []
            self.month_end_prices[symbol] = []

    def calculate_returns_list(self, symbol):
        integrated_list = zip(self.month_start_prices[symbol], self.month_end_prices[symbol])
        for begin_price, end_price in integrated_list:
            returns = get_returns_rate(new_value=end_price, init_value=begin_price)
            self.returns[symbol].append(returns)

    def is_same_month(self, symbol):
        return self.last_bars[symbol].getDateTime().month == self.current_bar.getDateTime().month

    def is_same_year(self, symbol):
        return self.last_bars[symbol].getDateTime().year == self.current_bar.getDateTime().year

    def is_month_begin(self, symbol):
        return self.last_bars[symbol].getDateTime().month != self.current_bar.getDateTime().month

    def is_end(self, bar):
        bar_dt = bar.getDateTime()
        return bar_dt.year == self.to_dt.year and bar_dt.month == self.to_dt.month and bar_dt.day == self.to_dt.day

    def update_month_start_list(self, symbol, bar):
        self.month_start_prices[symbol].append(bar.getClose())
        # self.write_to_excel(symbol, row=[bar.getDateTime(), bar.getClose()])

    def update_month_end_list(self, symbol, bar):
        self.month_end_prices[symbol].append(bar.getClose())
        # self.write_to_excel(symbol, row=[bar.getDateTime(), bar.getClose()])

    def write_to_excel(self, symbol, row, base_dir=LOG_DIR, filename='row_price'):
        path = os.path.join(base_dir, f'{filename}.xlsx')
        if os.path.exists(path):
            workbook = load_workbook(path)
        else:
            workbook = Workbook()
        if symbol not in workbook.get_sheet_names():
            worksheet = workbook.create_sheet(symbol)
            header = ['date', 'price']
            worksheet.append(header)
        else:
            worksheet = workbook.get_sheet_by_name(symbol)
        worksheet.append(row)
        workbook.save(path)

    def onBars(self, bars: Bars):
        # print(bars.getDateTime(),bars.getInstruments(), "??")
        # input()
        # print(bars(), "???")
        self.current_bar = bars[bars.getInstruments()[0]]  # type: Bar
        bar_dt = self.current_bar.getDateTime()
        # init data
        print(f'\ncurrent dt = {self.current_bar.getDateTime()} {self.is_end(self.current_bar)}')
        for symbol in self.symbols:
            bar = bars.getBar(symbol)
            if bar is None:
                continue
            if self.is_start[symbol]:
                self.update_month_start_list(symbol, bar)
                self.is_start[symbol] = False
            elif self.is_month_begin(symbol) and self.is_same_year(symbol):
                self.update_month_start_list(symbol, bar)
                self.update_month_end_list(symbol, self.last_bars.get(symbol))
            elif not self.is_same_year(symbol) or self.is_end(bar):
                assert len(self.month_start_prices) == len(self.month_end_prices), "len is not match"
                if self.is_end(bar):
                    self.update_month_end_list(symbol, bar)
                else:
                    self.update_month_end_list(symbol, self.last_bars.get(symbol))
                self.calculate_returns_list(symbol)
                self.reset(symbol=symbol)

                if not self.is_end(bar):
                    self.update_month_start_list(symbol, bar)
            self.last_bars[symbol] = bar

        if self.is_end(self.current_bar):
            service.write_correlation_excel('100_symbol_', sheet_name='', data_dict=self.returns)


def get_returns_rate(init_value, new_value):
    return (new_value - init_value) / init_value


if __name__ == '__main__':
    from_year = 2011
    to_year = 2020
    feed = PiTradingFeed(frequency=Frequency.DAY)
    symbol_year_list = service.get_pi_stock_list(end_year=to_year)
    for symbol, year in symbol_year_list:
        feed.addBarsAuto(instrument=symbol, from_year=from_year, to_year=to_year)
    sty = Correlation(
        feed=feed,
        cash_or_brk=1000000,
        from_dt=datetime(from_year, 1, 1),
        to_dt=datetime(to_year, 12, 18),
        symbols=[x[0] for x in symbol_year_list],
    )
    sty.run()
