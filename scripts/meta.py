from enum import Enum


class CategoryType(Enum):
    ETF = 'etfs'
    FOREX = 'forex'
    FUTURES = 'futures'
    INDICATORS = 'indicators'
    INDICES = 'indices'
    STOCKS = 'stocks'
