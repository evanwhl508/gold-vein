import logging
import os
import pathlib
from dataclasses import dataclass
from datetime import datetime
from decimal import Decimal
from typing import List

from openpyxl import load_workbook, Workbook

from backtest.pyalgotrade import service
from config import BASE_DIR, LOG_DIR

logger = logging.getLogger(__name__)
FILE_PATH = pathlib.Path(BASE_DIR, 'logs')

STRATEGIES = [
    "ATRChannelStrategy",
    "BollingerBreakout",
    "DonchianChannelStrategy",
    "DonchianChannelWithTimeExit",
    "DualMovingAverage",
    "TripleMovingAverage",
    "ChipScoreStrategy",
]


@dataclass
class ResultData:
    symbol: str
    invest_percentage: Decimal
    stop_loss_atr: int
    feed_start_datetime: datetime
    duration_years: int
    sma_day_score_day: int
    high_low_period: int
    short_sma_period: int
    long_sma_period: int
    trend_sma_period: int
    score_setting: str
    position_side: str
    max_draw_down: Decimal  # %
    longest_draw_down: int
    annual_return: Decimal  # %
    trading_number: int
    success_trade: Decimal  # %
    sharp_ratio: Decimal  # %
    cagr: Decimal  # %
    buy_and_hold: Decimal  # %


def get_files_path(input_folder):
    file_list = []
    for root, _, file_names in os.walk(input_folder):
        for file_name in file_names:
            ext = pathlib.Path(file_name).suffix
            if ext == '.xlsx':
                if '_' in file_name:
                    path = os.path.join(root, file_name)
                    file_list.append(path)
        break  # 只read第一層
    return file_list


def check_finished_symbol(file_list):
    s =[]
    for file_path in file_list:
        file_name = pathlib.Path(file_path).stem
        strategy, symbol = file_name.split('_')
        s.append(symbol)
    return {'symbols':s,'len':len(s)}


def read_excel(excel_path) -> List[ResultData]:
    workbook = load_workbook(excel_path)
    worksheet = workbook.active
    data = []
    for row in worksheet.iter_rows(min_row=2, max_row=None, min_col=None, max_col=None, values_only=True):
        symbol = row[0]
        buffer_data = ResultData(
            symbol=symbol,
            invest_percentage=row[1],
            stop_loss_atr=row[2],
            feed_start_datetime=row[3],
            duration_years=row[4],
            sma_day_score_day=row[5],
            high_low_period=row[6],
            short_sma_period=row[7],
            long_sma_period=row[8],
            trend_sma_period=row[9],
            score_setting=row[10],
            position_side=row[11],
            max_draw_down=row[12],
            longest_draw_down=row[13],
            annual_return=row[14],
            trading_number=row[15],
            success_trade=row[16],
            sharp_ratio=row[17],
            cagr=row[18],
            buy_and_hold=row[19]
        )
        data.append(buffer_data)
    return data


def gen_excel(file_list):
    symbol_list = [row for row in service.get_pi_stock_category_list().keys()]
    rows = [(symbol, strategy) for symbol in symbol_list for strategy in STRATEGIES]
    for symbol, strategy in rows:
        # read data
        file_path = os.path.join(LOG_DIR, f'{strategy}_{symbol}.xlsx')
        if file_path not in file_list:
            continue
        data = read_excel(file_path)
        file_name = pathlib.Path(file_path).stem
        strategy, symbol = file_name.split('_')

        # set final excel path
        summary_excel_name = f'{strategy}-AnnualReturn.xlsx'
        summary_excel_path = os.path.join(BASE_DIR, 'logs', summary_excel_name)

        # set header
        header = ''
        if os.path.exists(summary_excel_path):
            workbook = load_workbook(summary_excel_path)
        else:
            workbook = Workbook()
            header = ['Symbol']

            if strategy in ('ATRChannelStrategy', 'BollingerBreakout'):
                for d in data:
                    header.append(f'i.p{d.invest_percentage}\nstop loss {d.stop_loss_atr}\nsma{d.sma_day_score_day}\nside {d.position_side}')
            elif strategy in ('DonchianChannelStrategy', 'DonchianChannelWithTimeExit'):
                for d in data:
                    header.append(f'invest percentage{d.invest_percentage}\nstop loss {d.stop_loss_atr}\nhigh low period{d.high_low_period}\nside {d.position_side}')
            elif strategy == 'DualMovingAverage':
                for d in data:
                    header.append(f'invest percentage{d.invest_percentage}\ndma{d.short_sma_period},{d.long_sma_period}\nside {d.position_side}')
            elif strategy == 'TripleMovingAverage':
                for d in data:
                    header.append(f'invest percentage{d.invest_percentage}\ntma{d.short_sma_period},{d.long_sma_period},{d.trend_sma_period}\nside {d.position_side}')
            elif strategy == 'ChipScoreStrategy':
                for d in data:
                    header.append(f'i.p{d.invest_percentage}\nstop loss {d.stop_loss_atr}\ndays{d.sma_day_score_day}\nsetting\n{d.score_setting}\nside {d.position_side}')
            else:
                logger.warning(f"Unexpected Strategy !!!")
                return
        worksheet = workbook.active
        if header:
            worksheet.append(header)

        # write annual return in to the excel
        cagr_list = [symbol]
        for d in data:
            cagr_list.append(f'{d.cagr:.6s}')
        worksheet.append(cagr_list)
        logger.warning(f"{strategy}_{symbol} finished")
        workbook.save(summary_excel_path)


if __name__ == '__main__':
    a = get_files_path(FILE_PATH)
    # print(check_finished_symbol(a))
    gen_excel(a)
