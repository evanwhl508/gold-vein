import datetime

import matplotlib.pyplot as plt

from backtest.chip_score.chip_score import cal_chips_scores
from providers import binance

symbol = 'DOGEUSDT'

data = binance.get_all_record(symbol, binance.S_PATH, 1000, interval='1d')
for d in data:
    d.trade_volume = d.trade_volume*d.close_price

x_datatime = [datetime.datetime.fromtimestamp(i.close_time/1000.0) for i in data]
y_price = [i.close_price for i in data]

y_score, y_excel_king_vwap = cal_chips_scores(data,100)

print(y_score[-1])

# x_datatime=x_datatime[-60:]
# y_price=y_price[-60:]
# y_excel_king_vwap=y_excel_king_vwap[-60:]
# y_score=y_score[-60:]
fig, ax1 = plt.subplots()

ax1.set_xlabel('time (s)')
ax1.set_ylabel('Price', color='tab:red')
ax1.plot(x_datatime, y_price, color='tab:red', label=f'{symbol}-price')
ax1.plot(x_datatime, y_excel_king_vwap, color='green',label='excel_king_vwap')
ax1.tick_params(axis='y', labelcolor='tab:red')


ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis


ax2.set_ylabel('chip score', color='tab:blue')  # we already handled the x-label with ax1
ax2.plot(x_datatime, y_score, color='tab:blue',label='chip score', linestyle="--")
ax2.tick_params(axis='y', labelcolor='tab:blue')

ax2.plot(x_datatime, [0.5 for i in range(len(x_datatime))], color='black', linestyle="--")
ax2.plot(x_datatime, [0.8 for i in range(len(x_datatime))], color='black', linestyle="--")

fig.legend()
fig.tight_layout()
plt.show()

