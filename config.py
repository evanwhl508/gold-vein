import os
from collections import namedtuple

BASE_DIR = os.path.dirname(os.path.abspath(os.path.abspath(__file__)))

# logging.basicConfig(
#     level=logging.INFO,
#     format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
# )
FeedConfig = namedtuple('FeedConfig', ['instrument', 'from_year', 'to_year'])

LOG_DIR = os.path.join(BASE_DIR, "logs")
if not os.path.exists(LOG_DIR):
    os.mkdir(LOG_DIR)

HK_STOCK_DIR = os.path.join(BASE_DIR, 'hk_stock')
if not os.path.exists(HK_STOCK_DIR):
    os.mkdir(HK_STOCK_DIR)

FUTURES_DIR = os.path.join(BASE_DIR, 'futures')
if not os.path.exists(FUTURES_DIR):
    os.mkdir(FUTURES_DIR)

# DATA_DIR = os.path.join(BASE_DIR, "backtest", 'data')
# if not os.path.exists(DATA_DIR):
#     os.mkdir(DATA_DIR)
