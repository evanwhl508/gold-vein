import logging

import schedule
import time
from datetime import datetime

from broadcast_bot import constant, service
from broadcast_bot.tg import TgSender
from providers.binance import get_all_record, S_PATH
from services.calc import atr, high_low

logger = logging.getLogger(__name__)

sender = TgSender(constant.TEST_QAZ_BOT)


def send():
    symbols = service.get_available_symbols()
    text = get_crypto_atr(symbols)
    print(text)
    # sender.send_or_update_formatted(
    #     chat_id= constant.GROUP,
    #     seq=1,
    #     text=text
    # )
    a = sender.update_formatted(
        chat_id=constant.GROUP,
        message_id=15,
        text=text
    )
    print(a)


def get_crypto_atr(symbols):
    text = [f"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}", ""]
    for symbol in symbols:
        try:
            data = get_all_record(f'{symbol.upper()}USDT', S_PATH, interval='1d', skip_len=1000)
            atr_20, aatr_20 = atr(data, 20)
            high_22, low_22 = high_low(data, 22)
            high_55, low_55 = high_low(data, 55)
            text.extend([
                f"*{symbol.upper()}USDT*",
                f"Current Price : {data[-1].close_price:.5}",
                f"ATR 20 : {atr_20:.5}",
                f"AATR 20 : {round(aatr_20, 4) * 100:.3}%",
                f"High/Low 22 : {high_22:.5} / {low_22:.5}",
                f"High/Low 55 : {high_55:.5} / {low_55:.5}",
                f"",
            ])
        except Exception as e:
            print(e)
            text.extend([
                f"{symbol.upper()}USDT",
                f"Exception : {e}",
                f"",
            ])
    text = "\n".join(x for x in text if x is not None)
    return text


if __name__ == '__main__':
    # schedule.every().day.at("09:00").do(send)
    while True:
        # schedule.run_pending()
        time.sleep(1)  # wait 1 second
