import logging
import time

from broadcast_bot import constant
from broadcast_bot.tg import TgSender
from broadcast_bot.updater import get_text

logger = logging.getLogger(__name__)

sender = TgSender(constant.TEST_QAZ_BOT)

def send_score(symbol, current_score):
    text, score = get_text(symbol)
    last_day_score = float(score[0])
    latest_score = float(score[1])
    if last_day_score < 0.8 and latest_score and (current_score< 0.8 <= latest_score or current_score +1.0 <= latest_score):
        sender.send(text, constant.GROUP)
        print(f"send up {symbol}")
        print(text)

    elif last_day_score < 0.8 and latest_score and (current_score < 0.8 <= latest_score or current_score + 1.0 <= latest_score):
        sender.send(text, constant.GROUP)
        print(f"send down{symbol}")
        print(text)
    else:
        print('no send', symbol)
    print('--*--' * 10)
    return latest_score


if __name__ == '__main__':

    symbols = ['ETHUSDT', 'BTCUSDT']
    current_score_e = 0
    current_score_b = 0
    while True:
        # schedule.run_pending()
        for symbol in symbols:
            if symbol == 'ETHUSDT':
                current_score_e = send_score(symbol,current_score_e)

            else:
                current_score_b = send_score(symbol, current_score_b)

        time.sleep(60)  # wait 60 second