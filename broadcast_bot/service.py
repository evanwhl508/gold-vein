import logging

logger = logging.getLogger(__name__)

SYMBOLS = [
    'BTC',
    'ETH',
    'EOS',
    'BCH',
    "LTC",
    "ETC",
    "TRX",
    "XRP",
    "LINK",
    "ADA",
    "DOGE",
    "BNB",
]


def get_available_symbols():
    return SYMBOLS
