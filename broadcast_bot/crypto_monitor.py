import copy
import requests
from datetime import datetime

from telegram.ext import Updater, MessageHandler, Filters, CommandHandler

from indicators import chip_score, donchian_channel
from broadcast_bot import constant, service
from broadcast_bot.atr_updater import get_crypto_atr
from broadcast_bot.tg import TgSender
from providers import binance, yahoo
from providers.binance import S_PATH

sender = TgSender(constant.EVAN_PRIVATE_CRYPTO_BOT)


def get_chip_score(symbol, plot=False):
    raw_data = binance.get_all_record(symbol, S_PATH, 1000, interval='1d')
    if raw_data is None:
        text = "Wrong symbol."
        return text, None
    data_n = copy.deepcopy(raw_data)
    data_s = copy.deepcopy(raw_data)
    score_details_n, is_normal_err = chip_score.get_current_score(data_n, symbol, "normal", "crypto", plot)
    score_details_s, is_sakata_err = chip_score.get_current_score(data_s, symbol, "sakata", "crypto", plot)
    text = [
        f"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}", "",
        f"*{symbol}*",
    ]
    if not is_normal_err:
        current_price_n = score_details_n["current_price"]
        current_vwap_n = score_details_n["current_vwap"]
        scores_list_n = score_details_n["scores_list"]
        plot_path_n = score_details_n["plot_path"]
        text.extend([
            f"__normal__",
            f"Current Price :{current_price_n:.3f}",
            f"Chips VWAP :{current_vwap_n:.3f}",
            f"Last 10 days scores: {scores_list_n}",
            "",
        ])
    else:
        text.extend(["Normal chip score got error.", ""])
        plot_path_n = None
    if not is_sakata_err:
        current_price_s = score_details_s["current_price"]
        current_vwap_s = score_details_s["current_vwap"]
        scores_list_s = score_details_s["scores_list"]
        plot_path_s = score_details_s["plot_path"]
        text.extend([
            f"__sakata__",
            f"Current Price :{current_price_s:.3f}",
            f"Chips VWAP :{current_vwap_s:.3f}",
            f"Last 10 days scores: {scores_list_s}",
            "",
        ])
    else:
        text.extend(["Normal chip score got error.", ""])
        plot_path_s = None

    text = "\n".join(x for x in text if x is not None)
    return text, plot_path_n, plot_path_s


def get_donchian(symbol, plot=False):
    current_price, current_upper, current_lower, plot_path, is_err = donchian_channel.get_donchian(symbol, 20, 10, "crypto", plot)
    if not is_err:
        text = [f"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}", ""]
        text.extend([
            f"*{symbol}*",
            f"Current Price :{current_price:.3f}",
            f"Donchian Channel",
            f"Upper IN: {current_upper[0]:.3f}, Upper OUT: {current_upper[1]:.3f}",
            f"Lower IN: {current_lower[0]:.3f}, Lower OUT: {current_lower[1]:.3f}",
            "",
        ])
        text = "\n".join(x for x in text if x is not None)
    else:
        text = "Wrong symbol."
    return text, plot_path


# Define a few command handlers. These usually take the two arguments update and
# context. Error handlers also receive the raised TelegramError object in error.
def start(update, context):
    """Send a message when the command /start is issued."""
    update.message.reply_text('Hi!')


def help(update, context):
    """Send a message when the command /help is issued."""
    text = f"Command List:\n" \
           f"1.  /help\n" \
           f"2.  /chips\n" \
           f"2.2 /sakata\n" \
           f"3.  /donchian\n" \
           f"4.  /analyze\n" \
           f"\n\n" \
           f"Description:\n" \
           f"Currently only support ChipScore (default) and Donchian Channel, both can add '_plot' after the symbol to generate a graph.\n" \
           f"\n\n" \
           f"***Indicator Config***\n" \
           f"Chipscore: last 100 days\n" \
           f"Donchian: 20 days H/L for enter, 10 days H/L for exit\n"
    update.message.reply_text(text)


def chips(update, context):
    """Echo the user message."""
    update_information = update.message
    chat_id = update_information.chat_id
    user_id = update_information.from_user['id']
    user_name = update_information.from_user['first_name']
    from_user_text: str = update_information.text
    symbol = f"{context.args[0].upper()}USDT"
    if len(context.args) > 1:
        action = context.args[1].upper()
    else:
        action = None
    print(f"In chat(id: {chat_id}), {user_name}(id:{user_id}) ask for： {from_user_text}")

    if action == "PLOT":
        text, plot_path_n, plot_path_s = get_chip_score(symbol, plot=True)
        update.message.reply_text(text, quote=False)
        if plot_path_n:
            print(plot_path_n, '*'*10)
            update.message.reply_photo(open(plot_path_n, 'rb'), quote=False)
        if plot_path_s:
            print(plot_path_s, '*'*10)
            update.message.reply_photo(open(plot_path_s, 'rb'), quote=False)
    else:
        text, _, _ = get_chip_score(symbol)
        update.message.reply_text(text, quote=False)


def donchian(update, context):
    update_information = update.message
    chat_id = update_information.chat_id
    user_id = update_information.from_user['id']
    user_name = update_information.from_user['first_name']
    from_user_text: str = update_information.text
    symbol = f"{context.args[0].upper()}USDT"
    if len(context.args) > 1:
        action = context.args[1].upper()
    else:
        action = None
    print(f"In chat(id: {chat_id}), {user_name}(id:{user_id}) ask for： {from_user_text}")

    # normal reply
    if action == "PLOT":
        text, plot_path = get_donchian(symbol, plot=True)
        print(plot_path, '*'*10)
        update.message.reply_text(text, quote=False)
        update.message.reply_photo(open(plot_path, 'rb'), quote=False)
    else:
        text, plot_path = get_donchian(symbol)
        update.message.reply_text(text, quote=False)


def funding_rate(update, context):
    def format_data(datas):
        data_list = []
        for item in datas:
            data_list.append({'rate': item['fundingRate'], 'time': item['fundingTime']})
        return data_list

    def get_data(contract_code: str, path, end_time=None):
        """
        如果 startTime 和 endTime 都未发送, 返回最近 limit 条数据.
        如果 startTime 和 endTime 之间的数据量大于 limit, 返回 startTime + limit情况下的数据。
        """

        params = dict(
            symbol=contract_code,
            limit=1000,
        )

        if end_time:
            params.update(endTime=end_time)
        resp = requests.get(path, params=params)
        if not resp.status_code == 200:
            return None
        data = resp.json()
        return format_data(data)

    def get_all_datas(contract_code: str, path):
        datas = []
        end_time = None
        while True:
            resp = get_data(contract_code, path, end_time=end_time)

            if resp is None:
                break
            if len(resp) == 0: break

            end_time = resp[0]['time'] - 1

            datas = resp + datas

        if len(datas) == 0:
            return None
        raw_datas = {'contract_code': contract_code,
                     'platform': 'binance',
                     'datas': datas}
        return raw_datas

    PATH = 'https://fapi.binance.com/fapi/v1/fundingRate' # usdt
    PATH_COIN_BASE = 'https://dapi.binance.com/dapi/v1/fundingRate'
    paths = [PATH, PATH_COIN_BASE]
    coin = context.args[0]
    text = ""
    for path in paths:
        if path == PATH:
            contract_code = f"{coin.upper()}USDT"
        else:
            contract_code = f"{coin.upper()}USD_PERP"
        print(contract_code)
        raw_data = get_all_datas(contract_code, path)

        if raw_data == None:
            print(f'contract {contract_code} does not exits in Binance')
            print('\n')
            continue
        print(f'raw_data = {raw_data}')
        f_rate = f"{float(raw_data['datas'][-1]['rate']) * 100:.4f}"
        text += (f"***{contract_code}***\n"
                 f"funding rate = {f_rate}%\n\n")
    update.message.reply_text(text, quote=False)


def main():
    updater = Updater(constant.EVAN_PRIVATE_CRYPTO_BOT, use_context=True)
    print(f"updater = {updater.__dict__}")
    print(f"updater bot = {updater.bot}")
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))
    dp.add_handler(CommandHandler("chips", chips))
    dp.add_handler(CommandHandler("donchian", donchian))
    dp.add_handler(CommandHandler("funding_rate", funding_rate))
    # dp.add_handler(MessageHandler(filters=Filters.text & ~Filters.command, callback=echo))
    # dp.add_handler(MessageHandler(Filters.text, echo))

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
