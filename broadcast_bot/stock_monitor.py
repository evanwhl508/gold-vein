import copy
import csv
import os
import pathlib
import time
from datetime import datetime

from telegram.ext import Updater, MessageHandler, Filters, CommandHandler

from indicators import chip_score, donchian_channel
from broadcast_bot import constant, service
from broadcast_bot.atr_updater import get_crypto_atr
from broadcast_bot.tg import TgSender
from indicators.chip_score import cal_chips_scores
from providers import yahoo

sender = TgSender(constant.EVAN_PRIVATE_STOCK_BOT)


def get_chip_score(symbol, plot=False):
    raw_data = yahoo.get_all_record(symbol, interval='1d')
    if raw_data is None:
        text = "Wrong symbol."
        return text, None
    data_n = copy.deepcopy(raw_data)
    data_s = copy.deepcopy(raw_data)
    score_details_n, is_normal_err = chip_score.get_current_score(data_n, symbol, "normal", "us-stock", plot)
    score_details_s, is_sakata_err = chip_score.get_current_score(data_s, symbol, "sakata", "us-stock", plot)
    text = [
        f"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}", "",
        f"*{symbol}*",
    ]
    if not is_normal_err:
        current_price_n = score_details_n["current_price"]
        current_vwap_n = score_details_n["current_vwap"]
        scores_list_n = score_details_n["scores_list"]
        plot_path_n = score_details_n["plot_path"]
        text.extend([
            f"__normal__",
            f"Current Price :{current_price_n:.3f}",
            f"Chips VWAP :{current_vwap_n:.3f}",
            f"Last 10 days scores: {scores_list_n}",
            "",
        ])
    else:
        text.extend(["Normal chip score got error.", ""])
        plot_path_n = None
    if not is_sakata_err:
        current_price_s = score_details_s["current_price"]
        current_vwap_s = score_details_s["current_vwap"]
        scores_list_s = score_details_s["scores_list"]
        plot_path_s = score_details_s["plot_path"]
        text.extend([
            f"__sakata__",
            f"Current Price :{current_price_s:.3f}",
            f"Chips VWAP :{current_vwap_s:.3f}",
            f"Last 10 days scores: {scores_list_s}",
            "",
        ])
    else:
        text.extend(["Normal chip score got error.", ""])
        plot_path_s = None

    text = "\n".join(x for x in text if x is not None)
    return text, plot_path_n, plot_path_s


def get_text(symbol, plot=False):  # BTCUSDT
    current_price, excel_vwap, scores, plot_path, is_err = chip_score.update_score(symbol, "normal", "us-stock", plot)
    if not is_err:
        text = [f"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}", ""]
        text.extend([
            f"*{symbol}*",
            f"Current Price :{current_price:.3f}",
            f"Chips VWAP :{excel_vwap:.3f}",
            f"Last 10 days scores: {scores}",
            "",
        ])
        text = "\n".join(x for x in text if x is not None)
    else:
        text = "Wrong symbol."
    return text, plot_path


def get_sakata_text(symbol, plot=False):  # BTCUSDT
    current_price, excel_vwap, scores, plot_path, is_err = chip_score.update_score(symbol, "sakata", "us-stock", plot)
    if not is_err:
        text = [f"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}", ""]
        text.extend([
            f"*{symbol}*",
            f"Current Price :{current_price:.3f}",
            f"Chips VWAP :{excel_vwap:.3f}",
            f"Last 10 days scores: {scores}",
            "",
        ])
        text = "\n".join(x for x in text if x is not None)
    else:
        text = "Wrong symbol."
    return text, plot_path


def get_donchian_text(symbol, plot=False):
    current_price, current_upper, current_lower, plot_path, is_err = donchian_channel.get_donchian(symbol, 100, 50, "us-stock", plot)
    if not is_err:
        text = [f"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}", ""]
        text.extend([
            f"*{symbol}*",
            f"Current Price :{current_price:.3f}",
            f"Donchian Channel",
            f"Upper IN: {current_upper[0]:.3f}, Upper OUT: {current_upper[1]:.3f}",
            f"Lower IN: {current_lower[0]:.3f}, Lower OUT: {current_lower[1]:.3f}",
            "",
        ])
        text = "\n".join(x for x in text if x is not None)
    else:
        text = "Wrong symbol."
    return text, plot_path


# Define a few command handlers. These usually take the two arguments update and
# context. Error handlers also receive the raised TelegramError object in error.
def start(update, context):
    """Send a message when the command /start is issued."""
    update.message.reply_text('Hi!')


def error(update, context):
    """Send a message when the command /start is issued."""
    update.message.reply_text('Error.')


def help(update, context):
    """Send a message when the command /help is issued."""
    text = f"Command List:\n" \
           f"1.  /help\n" \
           f"2.  /chips\n" \
           f"2.2 /sakata\n" \
           f"3.  /donchian\n" \
           f"4.  /analyze\n" \
           f"\n\n" \
           f"Description:\n" \
           f"Currently only support ChipScore (default) and Donchian Channel, both can add '_plot' after the symbol to generate a graph.\n" \
           f"\n\n" \
           f"***Indicator Config***\n" \
           f"Chipscore: last 100 days\n" \
           f"Donchian: 100 days H/L for enter, 50 days H/L for exit\n"
    update.message.reply_text(text)


def analyze_stock_list(update, context):
    BASE_DIR = os.path.dirname(os.path.abspath(os.path.abspath(__file__)))
    ROOT_DIR = pathlib.Path(BASE_DIR).parent
    sender = TgSender(constant.EVAN_PRIVATE_STOCK_BOT)
    adj_symbol_list = []
    buy_list_n = []
    buy_list_s = []
    sell_list_n = []
    sell_list_s = []
    text = [f"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}"]
    code_len = 4
    stock_type = context.args[0].lower()
    if len(context.args) > 1:
        option = context.args[1].lower()
    else:
        option = None
    update.message.reply_text("Please wait...", quote=False)
    if stock_type == "hk":
        if option == "mine":
            source_list = ["MY_HK_RECOMMANDATION.csv", ]
        else:
            source_list = [
                "HongKongStockMainBoard.csv", "MY_HK_RECOMMANDATION.csv"
                     ]
        symbol_list = []
        for filename in source_list:
            path = os.path.join(ROOT_DIR, filename)
            with open(path, 'r', encoding='utf-8') as f:
                rows = csv.reader(f)
                symbol_list.extend([row[0] for idx, row in enumerate(rows) if idx != 0])
        for s in symbol_list:
            if len(s) < code_len:
                s = f"{'0' * (code_len - len(s))}{s}"
            s = f"{s}.HK"
            if s not in adj_symbol_list:
                adj_symbol_list.append(s)
    elif stock_type == "us":
        if option == "mine":
            source_list = ["MY_US_RECOMMANDATION.csv", ]
        else:
            source_list = [
                "ARK_INNOVATION_ETF_ARKK_HOLDINGS.csv", "ARK_NEXT_GENERATION_INTERNET_ETF_ARKW_HOLDINGS.csv",
                "ARK_GENOMIC_REVOLUTION_MULTISECTOR_ETF_ARKG_HOLDINGS.csv", "ARK_FINTECH_INNOVATION_ETF_ARKF_HOLDINGS.csv",
                "ARK_AUTONOMOUS_TECHNOLOGY_&_ROBOTICS_ETF_ARKQ_HOLDINGS.csv", "MY_US_RECOMMANDATION.csv"
                     ]
        for filename in source_list:
            path = os.path.join(ROOT_DIR, filename)
            with open(path, 'r', encoding='utf-8') as f:
                rows = csv.reader(f)
                symbol_list = [row[3].replace('"', '').split(" ")[0] for idx, row in enumerate(rows) if idx != 0 and row[3].replace('"', '').split(" ")[0] != '']
                adj_symbol_list.extend([s for s in symbol_list if s not in adj_symbol_list])

    # print(f"symbol_list = {symbol_list}, \nlen = {len(symbol_list)}")
    print(f"adj_symbol_list = {adj_symbol_list}, \nlen = {len(adj_symbol_list)}")
    for symbol in adj_symbol_list:
        symbol_text = f"*{symbol}*\n"
        is_fulfilled = False
        raw_data = yahoo.get_all_record(symbol, interval='1d')
        if raw_data is None:
            print(f"Wrong symbol.")
            continue
        data_n = copy.deepcopy(raw_data)
        data_s = copy.deepcopy(raw_data)

        # normal
        current_price_n = data_n[-1].close_price
        for d in data_n:
            d.trade_volume = d.trade_volume * d.close_price
        scores_n, excel_vwap_n = cal_chips_scores(data_n, 100, "normal")
        try:
            if scores_n[-2] < 0.8 <= scores_n[-1]:
                if not any([True for i in range(3, 11) if scores_n[-i] >= 0.8]):
                    symbol_text += f"__normal__\n" \
                                   f"Current Price :{current_price_n:.3f}\n" \
                                   f"Chips VWAP :{excel_vwap_n[-1]:.3f}\n" \
                                   f"Last 10 days scores: {[f'{s:.3f}' for s in scores_n[-10:]]}\n"
                    buy_list_n.append(symbol)
                    is_fulfilled = True
            elif scores_n[-2] > 0.5 >= scores_n[-1]:
                if not any([True for i in range(3, 11) if scores_n[-i] <= 0.5]):
                    symbol_text += f"__normal__\n" \
                                   f"Current Price :{current_price_n:.3f}\n" \
                                   f"Chips VWAP :{excel_vwap_n[-1]:.3f}\n" \
                                   f"Last 10 days scores: {[f'{s:.3f}' for s in scores_n[-10:]]}\n"
                    sell_list_n.append(symbol)
                    is_fulfilled = True

        except IndexError:
            text.append(f"{symbol} got index error.")
            pass

        # sakata
        current_price_s = data_s[-1].close_price
        for d in data_s:
            d.trade_volume = d.trade_volume * d.close_price
        scores_s, excel_vwap_s = cal_chips_scores(data_s, 100, "sakata")
        try:
            if scores_s[-2] < 0.8 <= scores_s[-1]:
                if not any([True for i in range(3, 11) if scores_s[-i] >= 0.8]):
                    symbol_text += f"*__sakata__*\n" \
                                   f"Current Price :{current_price_s:.3f}\n" \
                                   f"Chips VWAP :{excel_vwap_s[-1]:.3f}\n" \
                                   f"Last 10 days scores: {[f'{s:.3f}' for s in scores_s[-10:]]}\n"

                    buy_list_s.append(symbol)
                    is_fulfilled = True
            elif scores_s[-2] > 0.5 >= scores_s[-1]:
                if not any([True for i in range(3, 11) if scores_s[-i] <= 0.5]):
                    symbol_text += f"*__sakata__*\n" \
                                   f"Current Price :{current_price_s:.3f}\n" \
                                   f"Chips VWAP :{excel_vwap_s[-1]:.3f}\n" \
                                   f"Last 10 days scores: {[f'{s:.3f}' for s in scores_s[-10:]]}\n"

                    sell_list_s.append(symbol)
                    is_fulfilled = True
        except IndexError:
            text.append(f"{symbol} got index error.")
            pass
        if is_fulfilled:
            text.append(symbol_text)
    fulfilled_buy = f"{len(buy_list_n)} / {len(buy_list_s)}"
    fulfilled_sell = f"{len(sell_list_n)} / {len(sell_list_s)}"
    buy_list_n = ", ".join(x for x in buy_list_n if x is not None)
    buy_list_s = ", ".join(x for x in buy_list_s if x is not None)
    result = f"Fulfilled stocks (n/s): {fulfilled_buy}\n" \
             f"(Normal) Suggest Buy: {buy_list_n}\n" \
             f"(Sakata) Suggest Buy: {buy_list_s}\n" \
             f"\n" \
             f"Fulfilled stocks (n/s): {fulfilled_sell}\n" \
             f"(Normal) Suggest Sell: {sell_list_n}\n" \
             f"(Sakata) Suggest Sell: {sell_list_s}\n"
    update.message.reply_text(result, quote=False)
    for t in text:
        time.sleep(4)  # tg only allow a bot sending 20 msgs to the same group in one minute.
        # resp = sender.send(t, "-525447189")
        # print("Bot : ", resp)
        update.message.reply_text(t, quote=False)


def chips(update, context):
    """Echo the user message."""
    update_information = update.message
    chat_id = update_information.chat_id
    user_id = update_information.from_user['id']
    user_name = update_information.from_user['first_name']
    from_user_text: str = update_information.text
    symbol = context.args[0].upper()
    if len(context.args) > 1:
        action = context.args[1].upper()
    else:
        action = None
    print(f"In chat(id: {chat_id}), {user_name}(id:{user_id}) ask for： {from_user_text}")

    if action == "PLOT":
        text, plot_path_n, plot_path_s = get_chip_score(symbol, plot=True)
        update.message.reply_text(text, quote=False)
        if plot_path_n:
            print(plot_path_n, '*'*10)
            update.message.reply_photo(open(plot_path_n, 'rb'), quote=False)
        if plot_path_s:
            print(plot_path_s, '*'*10)
            update.message.reply_photo(open(plot_path_s, 'rb'), quote=False)
    else:
        text, _, _ = get_chip_score(symbol)
        update.message.reply_text(text, quote=False)


def donchian(update, context):
    update_information = update.message
    chat_id = update_information.chat_id
    user_id = update_information.from_user['id']
    user_name = update_information.from_user['first_name']
    from_user_text: str = update_information.text
    symbol = context.args[0].upper()
    if len(context.args) > 1:
        action = context.args[1].upper()
    else:
        action = None
    print(f"In chat(id: {chat_id}), {user_name}(id:{user_id}) ask for： {from_user_text}")

    # normal reply
    if action == "PLOT":
        text, plot_path = get_donchian_text(symbol, plot=True)
        print(plot_path, '*'*10)
        update.message.reply_text(text, quote=False)
        update.message.reply_photo(open(plot_path, 'rb'), quote=False)
    else:
        text, plot_path = get_donchian_text(symbol)
        update.message.reply_text(text, quote=False)


def main():
    updater = Updater(constant.EVAN_PRIVATE_STOCK_BOT, use_context=True)
    print(f"updater = {updater.__dict__}")
    print(f"updater bot = {updater.bot}")
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    # dp.add_error_handler(error)
    dp.add_handler(CommandHandler("help", help))
    dp.add_handler(CommandHandler("analyze", analyze_stock_list))
    dp.add_handler(CommandHandler("chips", chips))
    dp.add_handler(CommandHandler("donchian", donchian))
    # dp.add_handler(MessageHandler(filters=Filters.text & ~Filters.command, callback=echo))
    # dp.add_handler(MessageHandler(Filters.text, echo))

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
