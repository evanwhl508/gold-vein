from datetime import datetime

from telegram.ext import Updater, MessageHandler, Filters

from backtest.chip_score.chip_score import update_score
from broadcast_bot import constant, service
from broadcast_bot.atr_updater import get_crypto_atr
from broadcast_bot.service import SYMBOLS
from broadcast_bot.tg import TgSender

sender = TgSender(constant.TEST_QAZ_BOT)


def get_text(symbol,plot=False):  # BTCUSDT
    current_price, excel_vwap, scores, plot_path = update_score(symbol, "crypto", plot)
    text = [f"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}", ""]
    text.extend([
        f"*{symbol}*",
        f"Current Price :{current_price:.3f}",
        f"excel king vwap :{excel_vwap:.3f}",
        f"Last 10 days scores: {scores}",
        "",
    ])
    text = "\n".join(x for x in text if x is not None)
    return text, plot_path


def echo(update, context):
    """Echo the user message."""
    # print(f'{update.message.from_user.first_name}:{update.from_user.id}')
    # print(update.message)
    # print(f"From : {update.message.from_user['first_name']} id: {update.message.from_user['id']}")
    update_information = update.message
    chat_id = update_information.chat_id
    user_id = update_information.from_user['id']
    user_name = update_information.from_user['first_name']
    from_user_text = update.message.text.upper()
    print(f'chat id{chat_id}')
    print(f"{user_name} id({user_id})ask for：")
    print(f'{from_user_text}')

    # normal reply
    if from_user_text in SYMBOLS:
        symbol = f"{from_user_text}USDT"
        text, plot_path = get_text(symbol)
        resp = sender.send(text, chat_id)
        print("Bot : ", resp)
    elif from_user_text.replace('PLOT','').replace(' ','') in SYMBOLS:
        symbol = f"{from_user_text.replace('PLOT','').replace(' ','')}USDT"
        text, plot_path = get_text(symbol,plot=True)
        print(plot_path,'*'*10)
        resp = sender.send(text, chat_id)
        sender.send_photo(plot_path,chat_id)
        print("Bot : ", resp,'\n','Plot path: ',plot_path)
    elif from_user_text == 'ATR':
        symbols = service.get_available_symbols()
        text1 = get_crypto_atr(symbols)
        print(text1)
        # sender.send_or_update_formatted(
        #     chat_id= constant.GROUP,
        #     seq=1,
        #     text=text
        # )
        a = sender.send(
            chat_id=chat_id,
            text=text1
        )


def main():
    updater = Updater(constant.TEST_QAZ_BOT, use_context=True)
    dp = updater.dispatcher
    # dp.add_handler(CommandHandler('i_love_my_boss', i_love_my_boss))
    dp.add_handler(MessageHandler(Filters.text & ~Filters.command, echo))
    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
