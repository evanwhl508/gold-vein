import csv
import os
import pathlib
import time
from datetime import datetime

from backtest.chip_score.chip_score import cal_chips_scores
from broadcast_bot import constant
from broadcast_bot.tg import TgSender
from providers import yahoo

def analyze_stock_list(stock_type):
    BASE_DIR = os.path.dirname(os.path.abspath(os.path.abspath(__file__)))
    ROOT_DIR = pathlib.Path(BASE_DIR).parent
    sender = TgSender(constant.EVAN_PRIVATE_STOCK_BOT)
    adj_symbol_list = []
    suggest_list = []
    text = [f"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}"]
    code_len = 4
    stock_type = "us"
    if stock_type == "hk":
        path = os.path.join(ROOT_DIR, "HongKongStockMainBoard.csv")
        with open(path, 'r', encoding='utf-8') as f:
            rows = csv.reader(f)
            symbol_list = [row[0] for idx, row in enumerate(rows) if idx != 0]
        for s in symbol_list:
            if len(s) < 4:
                s = f"{'0' * (4 - len(s))}{s}"
            s = f"{s}.HK"
            adj_symbol_list.append(s)
    elif stock_type == "us":
        source_list = [
            "ARK_INNOVATION_ETF_ARKK_HOLDINGS.csv", "ARK_NEXT_GENERATION_INTERNET_ETF_ARKW_HOLDINGS.csv",
            "ARK_GENOMIC_REVOLUTION_MULTISECTOR_ETF_ARKG_HOLDINGS.csv", "ARK_FINTECH_INNOVATION_ETF_ARKF_HOLDINGS.csv",
            "ARK_AUTONOMOUS_TECHNOLOGY_&_ROBOTICS_ETF_ARKQ_HOLDINGS.csv",
                 ]
        for filename in source_list:
            path = os.path.join(ROOT_DIR, filename)
            with open(path, 'r', encoding='utf-8') as f:
                rows = csv.reader(f)
                adj_symbol_list.extend([row[3].replace('"', '').split(" ")[0] for idx, row in enumerate(rows) if idx != 0 and row[3].replace('"', '').split(" ")[0] != ''])

    # print(f"symbol_list = {symbol_list}, \nlen = {len(symbol_list)}")
    print(f"adj_symbol_list = {adj_symbol_list}, \nlen = {len(adj_symbol_list)}")
    for symbol in adj_symbol_list:
        data = yahoo.get_all_record(symbol, 1000, interval='1d')
        if data is None:
            print(f"Wrong symbol.")
            continue
        current_price = data[-1].close_price
        for d in data:
            d.trade_volume = d.trade_volume * d.close_price
        scores, excel_vwap = cal_chips_scores(data, 100)
        last_10_scores = [f'{score:.3f}' for score in scores[-10:]]
        print(f"symbol = {symbol}, scores[-1] = {scores[-1]}, scores[-2] = {scores[-2]}")
        if scores[-2] < 0.8 <= scores[-1]:
            if not any([True for i in range(3, 11) if scores[-i] >= 0.8]):
                text.append(
                    f"*{symbol}*\n"
                    f"Current Price :{current_price:.3f}\n"
                    f"Chips VWAP :{excel_vwap[-1]:.3f}\n"
                    f"Last 10 days scores: {[f'{score:.3f}' for score in scores[-10:]]}\n",
                )
                suggest_list.append(symbol)
    fulfilled_stock = len(suggest_list)
    suggest_list = ", ".join(x for x in suggest_list if x is not None)
    print(f"suggest_list = {suggest_list}")
    text.append(
        f"Fulfilled stocks: {fulfilled_stock}\n"
        f"Suggest Buy: {suggest_list}"
    )
    for t in text:
        resp = sender.send(t, "-525447189")
        print("Bot : ", resp)
        time.sleep(3.5)  # tg only allow a bot sending 20 msgs to the same group in one minute.

if __name__ == '__main__':
pass