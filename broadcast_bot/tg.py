from telegram import Bot, ParseMode


def format_text(text):
    lines = text.split("\n")
    # todo monospace size
    """ set align to center """
    new_data = []
    sep = " : "
    max_len = max(x.find(sep) for x in lines)
    for x in lines:
        if sep in x:
            space = " " * (max_len - x.find(sep))
            x = space + x
        if "*" not in x:
            x = f"`{x}`"
        new_data.append(x)
    return "\n".join(new_data)


class TgSender:

    buffer = {}

    def __init__(self, token):
        self.bot = Bot(token=token)

    def send(self, text, chat_id):
        return self.bot.send_message(
            chat_id=chat_id,
            text=text,
        )

    def send_photo(self,path, chat_id):
        self.bot.send_photo(
            photo=open(path, 'rb'),
            chat_id=chat_id,
        )

    def send_formatted(self, text, chat_id, align=True):
        if align:
            text = f"{format_text(text)}"
        return self.bot.send_message(
            chat_id=chat_id,
            text=text,
            parse_mode=ParseMode.MARKDOWN,
        )

    def update_formatted(self, text, chat_id, message_id, align=True):
        if align:
            text = f"{format_text(text)}"
        return self.bot.edit_message_text(
            chat_id=chat_id,
            message_id=message_id,
            text=text,
            parse_mode=ParseMode.MARKDOWN,
        )

    def send_or_update_formatted(self, text, chat_id, seq, align=True):
        key = f"{chat_id}_{seq}"
        if key not in self.buffer:
            sent_info = self.send_formatted(text, chat_id, align)
            print(sent_info)
            self.buffer[key] = sent_info['message_id']
        else:
            self.update_formatted(text, chat_id, self.buffer[key], align)


if __name__ == '__main__':
    pass
    # import constant
    # sender = TgSender(constant.FAT_FAT_BOT)
    # sender.send_formatted('*text*', chat_id=constant.COIN_CHAT, align=False)

