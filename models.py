from dataclasses import dataclass
from datetime import datetime
from decimal import Decimal
from enum import IntEnum
from operator import attrgetter
from typing import List


@dataclass
class Data:
    dt: datetime
    symbol: str
    open: Decimal
    close: Decimal
    high: Decimal
    low: Decimal
    volume: Decimal = 0
    last: Decimal = 0


@dataclass
class DataStream:
    raw_data_list: List[Data]
    data_list: List[Data] = None

    def __post_init__(self):
        self.raw_data_list.sort(key=attrgetter('dt'))
        self.data_list = self.raw_data_list.copy()

    def set_range(self, from_dt: datetime, to_dt: datetime):
        self.data_list = [x for x in self.raw_data_list if from_dt <= x.dt <= to_dt]

    @property
    def open_tick(self):
        return self.data_list[0]

    @property
    def close_tick(self):
        return self.data_list[-1]

    @property
    def highest(self):
        return max(x.high for x in self.data_list)

    @property
    def lowest(self):
        return min(x.low for x in self.data_list)

    @property
    def open(self):
        return self.open_tick.open

    @property
    def close(self):
        return self.close_tick.close

    @property
    def open_dt(self):
        return self.open_tick.dt

    @property
    def close_dt(self):
        return self.close_tick.dt


class Direction(IntEnum):
    BUY = 1
    SELL = -1


class OrderStatus(IntEnum):
    OPEN = 1
    FILLED = 2


@dataclass
class LimitOrder:
    symbol: str
    direction: Direction
    price: Decimal
    amount: Decimal
    order_dt: datetime
    filled_dt: datetime = None
    status: OrderStatus = OrderStatus.OPEN

    def execute(self, dt):
        assert self.status == OrderStatus.OPEN
        self.status = OrderStatus.FILLED
        self.filled_dt = dt

    @property
    def cash_amount(self):
        return self.price * self.amount

    @property
    def is_open(self):
        return self.status == OrderStatus.OPEN


@dataclass
class TradeLog:
    dt: datetime
    order_dt: datetime
    trade_amount: Decimal
    trade_price: Decimal
    direction: Direction
    cash_balance: Decimal
    asset_balance: Decimal

    @property
    def portfolio_value(self):
        return self.trade_price * self.asset_balance + self.cash_balance

    def as_dict(self):
        return dict(
            dt=self.dt.strftime('%Y-%m-%d %H:%M:%S'),
            order_dt=self.order_dt.strftime('%Y-%m-%d %H:%M:%S'),
            portfolio_value=str(self.portfolio_value),
            trade_amount=str(self.trade_amount),
            trade_price=str(self.trade_price),
            direction=self.direction.name,
            cash_balance=str(self.cash_balance),
            asset_balance=str(self.asset_balance),
        )
