import numpy


def true_range(current_high, current_low, previous_close=0):  # previous close = 0 if no prior data

    tr = max(abs(current_high - current_low),
             abs(current_high - previous_close),
             abs(current_low - previous_close))

    return tr


def atr(data, days):
    datas = data[-(days + 1):]
    tr_list = []

    for inx, _data in enumerate(datas):
        # print(_data)
        # print(index)
        if inx == 0: continue
        # print('current_high', _data.max_price)
        # print('current_low', _data.min_price)
        # print('current_close', _data.close_price)
        # print('previous_close', datas[inx - 1].close_price)
        tr = true_range(_data.max_price, _data.min_price, datas[inx - 1].close_price)
        tr_list.append(tr)
    atr = numpy.mean(tr_list)
    aatr = atr/datas[-1].close_price
    return atr, aatr


def high_low(data, days):
    datas = data[-days:]
    high = max(_data.max_price for _data in datas)
    low = min(_data.min_price for _data in datas)
    return high, low


if __name__ == '__main__':
    pass
