import csv
import logging
import os
import pickle
from dataclasses import dataclass
from datetime import datetime
from decimal import Decimal
from typing import List

import pandas
import requests
from pandas import DataFrame

from models import Data

F_USDT_PATH = 'https://fapi.binance.com/fapi/v1/klines'  # USDT future
F_COIN_PATH = 'https://dapi.binance.com/dapi/v1/klines'  # Coin future
S_PATH = 'https://api.binance.com/api/v3/klines'  # spot

logger = logging.getLogger(__name__)


@dataclass
class Record:
    """
    [
      [
        1499040000000,      // 开盘时间 0
        "0.01634790",       // 开盘价 1
        "0.80000000",       // 最高价 2
        "0.01575800",       // 最低价 3
        "0.01577100",       // 收盘价(当前K线未结束的即为最新价) 4
        "148976.11427815",  // 成交量 5
        1499644799999,      // 收盘时间 6
        "2434.19055334",    // 成交额 7
        308,                // 成交笔数 8
        "1756.87402397",    // 主动买入成交量 9
        "28.46694368",      // 主动买入成交额 10
        "17928899.62484339" // 请忽略该参数 11
      ]
    ]
    """
    start_time: int
    open_price: Decimal
    max_price: Decimal
    min_price: Decimal
    close_price: Decimal
    trade_volume: Decimal
    close_time: int
    quote_asset_volume: str
    number_of_trades: str
    taker_buy_base_asset_volume: str
    taker_buy_quote_asset_volume: str
    ignore: str
    symbol: str

    def __post_init__(self):
        self.start_time = int(self.start_time)
        self.open_price = Decimal(self.open_price)
        self.max_price = Decimal(self.max_price)
        self.min_price = Decimal(self.min_price)
        self.close_price = Decimal(self.close_price)
        self.trade_volume = Decimal(self.trade_volume)
        self.close_time = int(self.close_time)


def get_data(symbol: str, path, end_time=None, interval='1m'):
    # print(symbol.upper())
    params = dict(
        symbol=symbol.upper(),
        interval=interval,
        limit=1000,

    )
    if end_time:
        params.update(endTime=end_time)
    resp = requests.get(path, params=params)
    # assert resp.status_code == 200, f"{resp.status_code}, {resp.text}"
    if not resp.status_code == 200:
        print(f"{symbol.upper()},{resp.status_code}, {resp.text}")
        return None
    datas = resp.json()
    data_list = [
        Record(data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8], data[9], data[10],
               data[11], symbol.upper()) for data in datas]
    # data_list = [{"star_time": data[0], "close_price": data[4], "close_time": data[6]} for data in datas]

    return data_list


def myFunc(e: Record):  # 上網抄
    return e.close_time


def get_all_record(symbol: str, path, skip_len: int = None, interval='1m', data_type=Record):
    datas = []
    end_time = None
    # a= 1
    while True:
        # print(a)
        resp = get_data(symbol, path, end_time=end_time, interval=interval)
        # a += 1
        if resp is None:
            return None
        if len(resp) == 0: break

        end_time = resp[0].start_time - 1
        for item in resp:
            if item not in datas:
                datas.append(item)
        if skip_len:
            if len(datas) >= skip_len: break  # 多過幾多個data就 break

    # datas.sort(key=myFunc)
    if data_type == Record:
        pass
    elif data_type == DataFrame:
        data_list = [[r.open_price, r.max_price, r.min_price, r.close_price, r.trade_volume] for r in datas]
        index = [datetime.fromtimestamp(r.start_time / 1000).strftime('%m/%d/%Y') for r in datas]
        df = pandas.DataFrame(data_list, columns=['Open', 'High', 'Low', 'Close', 'Volume'], index=index)
        datas = df
    else:
        raise ValueError("Wrong data type")
    return datas


def save_spot_data(raw_data: List[Record]):
    file = os.path.join('binance_spot_datas', f'{raw_data[0].symbol.lower()}.pkl')
    with open(file, 'wb') as f:
        pickle.dump(raw_data, f)


def load_spot_data(symbol: str) -> List[Record]:
    for root, _, file_names in os.walk('binance_spot_datas'):
        for file_name in file_names:
            if f'{symbol.lower()}' in file_name:
                data_path = os.path.join(root, file_name)
                with open(data_path, 'rb') as f:
                    raw_data: List[Record] = pickle.load(f)
                    return raw_data

        print(f'There are no data of {symbol}')
        return None


def parse(raw: Record):
    return Data(
        dt=datetime.fromtimestamp(raw.close_time / 1000),
        open=Decimal(raw.open_price),
        close=Decimal(raw.close_price),
        high=Decimal(raw.max_price),
        low=Decimal(raw.min_price),
        symbol=raw.symbol,
    )


def export_to_csv(symbol, rows: List[Record]):
    path = f'../binance_spot_datas/{symbol}.csv'
    if os.path.exists(path):
        return
    with open(f'{path}', 'w', newline='') as csv_file:
        writer = csv.writer(csv_file, delimiter=',')
        writer.writerow(('Date', 'Time', 'Open', 'High', 'Low', 'Close', 'Volume'))
        for row in rows:
            writer.writerow((
                datetime.fromtimestamp(row.start_time / 1000).strftime('%m/%d/%Y'),
                datetime.fromtimestamp(row.start_time / 1000).strftime('%H%M'),
                row.open_price, row.max_price,
                row.min_price, row.close_price,
                row.trade_volume))


if __name__ == '__main__':
    # feed = BinanceFeed()
    # path = os.path.join(BASE_DIR, 'binance_spot_datas', 'ADAUSDT.csv')
    # feed.addBarsFromCSV(instrument='ADA', path=path)
    #
    spot_record = get_all_record('btcbusd', S_PATH, skip_len=43200, interval='1d', data_type=DataFrame)  # 半年：262800 1個月：43200
    # save_spot_data(spot_record)
    print(f"spot_record = {spot_record}")

    # data = load_spot_data('btcbusd')
    # if data is not None:
    #     # print(data)
    #     print(' symbol: ', data[0].symbol)
    #     print(' len of data: ', len(data))
    #     print(' date: ', datetime.fromtimestamp(data[0].close_time / 1000.0))
    #     print(' open: ', data[0].open_price)
    #     print(' high: ', data[0].max_price)
    #     print(' low: ', data[0].min_price)
    #     print(' close: ', data[0].close_price)
    # symbols = ['DOT', 'LINK', 'ADA', 'BNB', 'BCH', 'ETC', 'BTC', 'ETH', 'LTC', 'XRP']
    # for symbol in symbols:
    #     data = get_all_record(f'{symbol}USDT', S_PATH, 100000, interval='1d')
    #     export_to_csv(f'{symbol}USDT', data)
