from datetime import datetime
from decimal import Decimal

import requests

from models import Data

"""
https://www.alphavantage.co/documentation/#intraday-extended
"""


def parse(raw, symbol):
    return Data(
        symbol=symbol,
        dt=datetime.strptime(raw['time'], '%Y-%m-%d %H:%M:%S'),
        open=Decimal(raw['open']),
        high=Decimal(raw['high']),
        low=Decimal(raw['low']),
        close=Decimal(raw['close']),
        volume=Decimal(raw['volume']),
    )


def run():
    key = 'FNJ9DIYACZ5ANFTH'
    url = 'https://www.alphavantage.co/query'
    params = dict(
        function='TIME_SERIES_INTRADAY_EXTENDED',
        symbol='AAPL',
        interval='1min',
        slice='year1month1',
        apikey=key,
    )
    resp = requests.get(url, params=params)
    print(resp.text)


if __name__ == '__main__':
    run()
