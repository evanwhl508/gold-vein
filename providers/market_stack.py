import json

import requests

"""
https://marketstack.com/documentation
"""


def run():
    key = '8af6002c72ab359a6df225794775af4e'
    url = 'http://api.marketstack.com/v1/intraday'
    params = dict(
        access_key=key,
        symbols='AAPL',
        interval='15min',
    )
    resp = requests.get(url, params=params)
    print(json.dumps(resp.json(), indent=2))

    url = 'http://api.marketstack.com/v1/exchanges'
    params = dict(
        access_key=key,
    )
    resp = requests.get(url, params=params)
    print(json.dumps(resp.json(), indent=2))


if __name__ == '__main__':
    run()
