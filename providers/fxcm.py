# note: install specific library version  (install library with order. manual install if install fail)
import sys
from datetime import datetime

import engineio
import fxcmpy
import socketio


# https://www.fxcm.com/fxcmpy/appendix.html#
# 因為經常沒有自動斷開, 所以寫咗一個 decorator 來處理
def disconnect(func):
    def wrapper(*args, **kwargs):
        data_list = func(*args, **kwargs)
        socket_io = args[0]  # type: Socket
        socket_io.socket.socket.disconnect()
        return data_list

    return wrapper


class Socket:
    TRADING_API_URL = 'https://api-demo.fxcm.com'
    WEBSOCKET_PORT = 443

    def __init__(self, token):
        self.token = token
        self.socket = fxcmpy.fxcmpy(
            access_token=ACCESS_TOKEN,
            log_level='debug',
            server='demo', log_file='log.txt')

    def on_connect(self):
        print('Websocket connected')

    def on_close(self):
        print('Websocket closed.')

    def on_error(self, str):
        print('Websocket error.' + str)

    def check_connection(self):
        print("Python version: " + sys.version)

        socketIO = socketio.Client()
        print("socket.io version: " + socketio.__version__)
        print("python-engineio version: " + engineio.__version__)
        socketIO.connect(self.TRADING_API_URL + ":" + str(self.WEBSOCKET_PORT) + "/?access_token=" + ACCESS_TOKEN)

        print(socketIO.eio.sid)
        bearer_access_token = "Bearer " + socketIO.eio.sid + ACCESS_TOKEN

        print(bearer_access_token)
        socketIO.on('disconnect', self.on_close)
        socketIO.on('connect', self.on_connect)
        socketIO.on('connect_error', self.on_error)
        socketIO.disconnect()

    def get_fx_list(self):
        return self.socket.get_instruments()

    def get_candles(self, exchange_pair, period='m1', return_size=250,
                    start: str = None, end: str = None):
        return self.socket.get_candles(exchange_pair, period=period,
                                       number=return_size, start=start, end=end)

    def get_fx_historical_list(self):
        return self.socket.get_instruments_for_candles()


if __name__ == '__main__':
    ACCESS_TOKEN = "f9c2214433ac0f7a25f8fc295926a2dbdcd10a5f"
    socket = Socket(ACCESS_TOKEN)
    # YYYY-MM-DD hh:mm
    start = datetime(2015, 1, 1, 8, 30, 0)
    end = datetime(2015, 1, 1, 10, 30, 0)
    data = socket.get_candles('AUD/USD', start=start, end=end)
    print(data)
