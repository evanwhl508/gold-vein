import os
import yfinance as yf
from dataclasses import dataclass
from decimal import Decimal
from pandas import DataFrame


@dataclass
class Record:
    """
    [
      [
        1499040000000,      // 开盘时间 0
        "0.01634790",       // 开盘价 1
        "0.80000000",       // 最高价 2
        "0.01575800",       // 最低价 3
        "0.01577100",       // 收盘价(当前K线未结束的即为最新价) 4
        "148976.11427815",  // 成交量 5
        1499644799999,      // 收盘时间 6
        "2434.19055334",    // 成交额 7
        308,                // 成交笔数 8
        "1756.87402397",    // 主动买入成交量 9
        "28.46694368",      // 主动买入成交额 10
        "17928899.62484339" // 请忽略该参数 11
      ]
    ]
    """
    start_time: int
    open_price: Decimal
    max_price: Decimal
    min_price: Decimal
    close_price: Decimal
    trade_volume: Decimal
    close_time: int
    quote_asset_volume: str
    number_of_trades: str
    taker_buy_base_asset_volume: str
    taker_buy_quote_asset_volume: str
    ignore: str
    symbol: str

    def __post_init__(self):
        self.start_time = int(self.start_time)
        self.open_price = Decimal(self.open_price)
        self.max_price = Decimal(self.max_price)
        self.min_price = Decimal(self.min_price)
        self.close_price = Decimal(self.close_price)
        self.trade_volume = Decimal(self.trade_volume)
        self.close_time = int(self.close_time)


def myFunc(e: Record):  # 上網抄
    return e.close_time


def get_all_record(symbol: str, interval='1m', data_type=Record):
    rows = yf.Ticker(symbol)
    resp = rows.history(period='20y', auto_adjust=False,
                        start='2002-01-01', rounding=True)  # type: DataFrame
    if resp.empty:
        return None
    if data_type == Record:
        data_list = [
            Record(
                start_time=index.timestamp(),
                open_price=row['Open'],
                max_price=row['High'],
                min_price=row['Low'],
                close_price=row['Close'],
                trade_volume=row['Volume'],
                close_time=index.timestamp(),
                quote_asset_volume="",
                number_of_trades="",
                taker_buy_base_asset_volume="",
                taker_buy_quote_asset_volume="",
                ignore="",
                symbol=symbol.upper()
            ) for index, row in resp.iterrows()
        ]

        data_list.sort(key=myFunc)
    elif data_type == DataFrame:
        data_list = resp
    else:
        raise ValueError("Wrong data type")
    return data_list


def download_data(symbol_list: list, target_path):
    for symbol in symbol_list:
        rows = yf.Ticker(symbol)
        data = rows.history(period='20y', auto_adjust=False,
                            start='2002-01-01', rounding=True)  # type: DataFrame
        if len(data) <= 0:
            continue
        data.to_csv(os.path.join(target_path, f'{symbol}.csv'))


if __name__ == '__main__':
    record = get_all_record("0005.HK", data_type=DataFrame)
    print(f"record = {record}")
